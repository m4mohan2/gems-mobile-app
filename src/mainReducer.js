import { combineReducers } from 'redux';

const INITIAL_STATE = {
    //CHANG IN PRODUCTION
    RELEASE_API_URL: "http://54.147.235.207/gems_uat/service/public/index.php/",
    USER_DETAILS: {}, 
    IMAGE_PREFIX_URL : "http://54.147.235.207/gems_uat/service/public/upload/profile/"
};

const reducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'UPDATE_USER_DETAILS':
            return {
                ...state,
                USER_DETAILS: action.payload
            }
        case 'DECREASE_COUNTER':
            return { counter: state.counter - 1 }
    }
    return state
}

export default combineReducers({
    appdata: reducer,
});