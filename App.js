/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component, Fragment} from 'react';
import SplashScreen from 'react-native-splash-screen';
import AppNavigator from './Components/AppNavigator';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import mainReducer from './src/mainReducer';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';

const store = createStore(mainReducer);

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userData: null,
      fetchComplete: false,
    };
  }
  componentDidMount() {
    // do stuff while splash screen is shown
    // After having done stuff (such as async tasks) hide the splash screen

    AsyncStorage.getItem('userInfo').then(
      (value) => {
        console.log('nnnn', value);
        this.setState({userData: value, fetchComplete: true}, () =>
          SplashScreen.hide(),
        );
      },
      //AsyncStorage returns a promise so adding a callback to get the value

      //Setting the value in Text
    );
  }
  render() {
    console.log('?????', this.state.fetchComplete);
    return this.state.fetchComplete == true ? (
      <Provider store={store}>
        <StatusBar barStyle="light-content" backgroundColor="black" />
        <AppNavigator userInfo={this.state} />
      </Provider>
    ) : (
      <View />
    );
  }
}

export default App;
