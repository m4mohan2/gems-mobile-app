import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Modal,
  TextInput,
  TouchableOpacity,
  Image,
  Alert,
  Linking,
} from 'react-native';

export default class CustAleartPage extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {}

  selectedOption = (value) => {
    this.props.updateAleartParent(value);
  };

  render() {
    return (
      <Modal transparent={true}>
        <View style={styles.View_1dropDown}>
          <View
            style={{
              width: '90%',
              backgroundColor: 'white',
              justifyContent: 'center',
              alignSelf: 'center',
              padding: 40,
              marginTop: 50,
              marginBottom: 50,
            }}>
            <TouchableOpacity
              style={{
                height: 40,

                borderRadius: 10,
                backgroundColor: '#F2F5F8',
                borderWidth: 1,
                borderColor: '#D2DAE3',
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() =>this.selectedOption(1)}>
              <Text
                style={{
                  color: '#3D73DD',
                  fontSize: 17,
                  fontFamily: 'NunitoSans-Regular',
                }}>
                Capture from camera
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                height: 40,
                marginTop: 20,
                borderRadius: 10,
                backgroundColor: '#F2F5F8',
                borderWidth: 1,
                borderColor: '#D2DAE3',
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() =>this.selectedOption(2)}>
              <Text
                style={{
                  color: '#3D73DD',
                  fontSize: 17,
                  fontFamily: 'NunitoSans-Regular',
                }}>
                Choose from galary
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                height: 40,
                marginTop: 20,
                borderRadius: 10,
                backgroundColor: '#F2F5F8',
                borderWidth: 1,
                borderColor: '#D2DAE3',
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() =>this.selectedOption(3)}>
              <Text
                style={{
                  color: '#3D73DD',
                  fontSize: 17,
                  fontFamily: 'NunitoSans-Regular',
                }}>
                Cancel
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  View_1dropDown: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    //width: 200,
    //height: 200,
    backgroundColor: 'rgba(52, 52, 52, 0.6)',
    justifyContent: 'center',
  },
  Lbl_Style: {
    //color: '#6d6b6b',
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
  },
});
