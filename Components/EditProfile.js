import React, {Component} from 'react';

import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  TextInput,
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  BackHandler,
  ToastAndroid,
} from 'react-native';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import AnimateLoadingButton from 'react-native-animate-loading-button';
import {Formik} from 'formik';
import * as Yup from 'yup';
import ImagePicker from 'react-native-image-crop-picker';
import CustomAlert from './CustomAlert';
import CustomModal from './CustomModal';

const initialValues = {
  first_name: '',
  last_name: '',
};

const validationRegister = Yup.object().shape({
  first_name: Yup.string().required('The first name field is required.'),
  last_name: Yup.string().required('The last name field is required.'),
});

class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profileInfo: {},
      isLoading: true,
      userInfo: this.props.route.params.userInfo,
      CustomAlertShow: false,
      profilePic: null,
      showCustomModal: false,
      AlertValue: {},
      count: 0,
    };
    console.log('edit', this.props.route.params.userInfo);
  }

  redirectChangePassword = () => {
    this.props.navigation.navigate('ChangePassword');
  };

  update_Custom_modal = () => {
    this.setState({showCustomModal: false});
  };

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButton.bind(this),
    );
  }
  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButton.bind(this),
    );
  }
  handleBackButton() {
    if (this.state.count == 0) {
      this.setState({count: this.state.count + 1});
      ToastAndroid.show('press back again to exist the app', 300);

      setTimeout(() => {
        this.setState({count: 0});
      }, 10000);
      //   let timer = setInterval(this.tick, 10);
      // this.setState({timer});
      return true;
    } else {
      BackHandler.exitApp();
    }
  }
  update_Aleart = (value) => {
    this.setState({CustomAlertShow: false});
    console.log(value);

    if (value == 1) {
      ImagePicker.openCamera({
        width: 300,
        height: 400,
        cropping: true,
      }).then((image) => {
        console.log(image);
        this.setState({profilePic: image});
      });
    } else if (value == 2) {
      ImagePicker.openPicker({
        width: 300,
        height: 400,
        cropping: true,
        includeBase64: true,
      }).then((image) => {
        console.log(image);
        this.setState({profilePic: image});
      });
    }
  };
  changeImage = () => {
    // ImagePicker.clean()
    //   .then(() => {
    //     console.log('removed all tmp images from tmp directory');
    //   })
    //   .catch((e) => {
    //     alert(e);
    //   });
    // /*
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
    }).then((image) => {
      console.log(image);
    });
   
    this.setState({CustomAlertShow: true});
  };

  componentWillUnmount() {
    // ImagePicker.clean()
    //   .then(() => {
    //     console.log('removed all tmp images from tmp directory');
    //   })
    //   .catch((e) => {
    //     alert(e);
    //   });
  }

  getProfile = (jsonValue) => {
    fetch(this.props.appdata.RELEASE_API_URL + 'api/profileDetails', {
      method: 'POST',
      headers: {
        token: 'Bearer ' + jsonValue.token,
      },
      body: JSON.stringify({
        user_id: jsonValue.user.id,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('++++', responseJson);
        jsonValue.user = responseJson.user;
        this.props.updateUser(responseJson.user);

        AsyncStorage.setItem('userInfo', JSON.stringify(jsonValue));

        this.joinNowButton.showLoading(false);

        this.setState({
          showCustomModal: true,
          AlertValue: {
            AlertHedingTitle: 'gems',
            AlertTitle: 'Your profile updated successfully',
            AlertCancleTitle: 'GO BACK',
            success: true,
            AlertOkTitle: 'OK',
            SingleButton: true,
          },
        });
      })
      .catch((error) => {
        this.setState({
          showCustomModal: true,
          AlertValue: {
            AlertHedingTitle: 'gems',
            AlertTitle: 'Something Went Wrong!\nPlease try after some time.',
            AlertCancleTitle: 'GO BACK',
            success: false,
            AlertOkTitle: 'OK',
            SingleButton: true,
          },
        });
      });
  };
  render() {
    const newInitialValues = Object.assign(initialValues, {
      first_name: this.props.appdata.USER_DETAILS.user_detail.first_name
        ? this.props.appdata.USER_DETAILS.user_detail.first_name
        : '',
      last_name: this.props.appdata.USER_DETAILS.user_detail.last_name
        ? this.props.appdata.USER_DETAILS.user_detail.last_name
        : '',
    });

    return (
      <SafeAreaView style={{flex: 1,backgroundColor: '#222529'}}>
        <KeyboardAvoidingView
          style={{flex: 1}}
          behavior={Platform.OS == 'ios' ? 'padding' : 'height'}>
          <ScrollView style={{}}>
            <View style={{flex: 1, backgroundColor: '#222529'}}>
              <View
                style={{
                  flexDirection: 'row',
                  //justifyContent: 'space-between',
                  //backgroundColor: 'blue',
                }}>
                {/* <TouchableOpacity
                  style={{
                    position: 'absolute',
                    marginLeft: 20,
                    height: 20,
                    width: 30,
                    marginTop: 41,
                    //backgroundColor: 'red',
                  }}
                  onPress={() => console.log('jjj')}></TouchableOpacity> */}
                <Text
                  style={{
                    flex: 1,
                    textAlign: 'center',
                    color: 'white',
                    fontSize: 32,
                    marginTop: 22,
                    alignSelf: 'center',
                    fontFamily: 'NunitoSans-ExtraBold',
                  }}>
                  gems
                </Text>
                <TouchableOpacity
                  style={{
                    position: 'absolute',
                    marginLeft: 20,
                    height: 20,
                    width: 30,
                    marginTop: 41,
                    //backgroundColor: 'red',
                  }}
                  onPress={() => {
                    this.props.navigation.navigate('Profile');
                  }}>
                  <Image
                    style={{height: 20, width: 20}}
                    source={require('../Images/back.png')}
                    resizeMode="contain"></Image>
                </TouchableOpacity>
              </View>
              <Formik
                initialValues={newInitialValues}
                validationSchema={validationRegister}
                onSubmit={(values, actions) => {
                  this.joinNowButton.showLoading(true);
                  console.log(values);
                  console.log(
                    this.props.appdata.RELEASE_API_URL + 'api/updateUser',
                  );
                  AsyncStorage.getItem('userInfo').then((value) => {
                    const jsonValue = JSON.parse(value);
                    console.log(jsonValue.token);
                    var postData = new FormData();
                    postData.append('first_name', values.first_name);
                    postData.append('last_name', values.last_name);

                    if (this.state.profilePic !== null) {
                      postData.append('profile_pic', {
                        uri: this.state.profilePic.path,
                        type: this.state.profilePic.mime,
                        name: 'ProfilePic',
                        data: this.state.profilePic.data,
                      });
                    }

                    fetch(
                      this.props.appdata.RELEASE_API_URL + 'api/updateUser',
                      {
                        method: 'POST',
                        headers: {
                          token: 'Bearer ' + jsonValue.token,
                          'Content-Type': 'multipart/form-data',
                        },
                        body: postData,
                      },
                    )
                      .then((response) => response.json())
                      .then((responseJson) => {
                        console.log('++++', responseJson);
                        this.getProfile(jsonValue);
                      })
                      .catch((error) => {
                        this.joinNowButton.showLoading(false);
                        this.setState({
                          showCustomModal: true,
                          AlertValue: {
                            AlertHedingTitle: 'gems',
                            success: false,
                            AlertTitle:
                              'Something Went Wrong!\nPlease try after some time.',
                            AlertCancleTitle: 'GO BACK',
                            AlertOkTitle: 'OK',
                            SingleButton: true,
                          },
                        });
                      });
                  });
                }}
                validationSchema={validationRegister}>
                {(formikProps) => (
                  <View
                    style={{
                      flex: 1,
                      backgroundColor: '#F2F5F8',
                      marginTop: 14,
                      borderTopLeftRadius: 25,
                      borderTopRightRadius: 25,
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity onPress={() => this.changeImage()}>
                        <Image
                          style={{
                            width: 70,
                            height: 70,
                            marginLeft: 20,
                            borderRadius: 70 / 2,
                            marginTop: 20,
                          }}
                          //source={require('../Images/Settings.png')}
                          //resizeMode="contain"
                          //backgroundColor="red"
                          source={
                            this.state.profilePic == null
                              ? this.props.appdata.USER_DETAILS.user_detail
                                  .profile_pic == null
                                ? require('../Images/defaultProfile.png')
                                : {
                                    uri:
                                      this.props.appdata.IMAGE_PREFIX_URL +
                                      this.props.appdata.USER_DETAILS
                                        .user_detail.profile_pic,
                                  }
                              : {uri: this.state.profilePic.path}
                          }></Image>
                        <Image
                          style={{
                            position: 'absolute',
                            height: 30,
                            width: 30,
                            borderRadius: 25 / 2,
                            //backgroundColor: 'red',
                            left: 76,
                            top: 40,
                          }}
                          source={require('../Images/camProfile.png')}></Image>
                      </TouchableOpacity>
                      <Text
                        style={{
                          fontSize: 17,
                          color: '#0F2146',
                          fontFamily: 'NunitoSans-Regular',
                          marginTop: 40,
                          marginLeft: 30,
                        }}
                        onPress={() => this.changeImage()}>
                        Change picture
                      </Text>
                    </View>

                    <TextInput
                      style={RegisterStyles.inputStyleBox}
                      placeholderTextColor="#0F2146"
                      placeholder="First name"
                      //underlineColorAndroid="rgba(0,0,0,0)"
                      keyboardType="default"
                      returnKeyType="next"
                      fontFamily="NunitoSans-Regular"
                      //onChangeText={(text) => this.setState({userName: text})}
                      value={formikProps.values.first_name}
                      onChangeText={formikProps.handleChange('first_name')}
                      onBlur={formikProps.handleBlur('first_name')}
                      blurOnSubmit={false}
                      autoCorrect={false}
                      disableFullscreenUI={true}
                      onSubmitEditing={() => {
                        this.secondTextInput.focus();
                      }}

                      //onBlur={() => this._CheckTConditionChacked()}
                      //onFocus={() => this._onFocusChacked()}
                    />

                    {formikProps.touched.first_name &&
                    formikProps.errors.first_name ? (
                      <Text style={RegisterStyles.errMsg}>
                        {formikProps.touched.first_name &&
                          formikProps.errors.first_name}
                      </Text>
                    ) : null}

                    <TextInput
                      ref={(input) => {
                        this.secondTextInput = input;
                      }}
                      style={RegisterStyles.inputStyleBox}
                      placeholderTextColor="#0F2146"
                      placeholder="Last name"
                      //underlineColorAndroid="rgba(0,0,0,0)"
                      keyboardType="default"
                      returnKeyType="next"
                      //onChangeText={(text) => this.setState({userName: text})}
                      value={formikProps.values.last_name}
                      autoCorrect={false}
                      disableFullscreenUI={true}
                      onSubmitEditing={() => {
                        this.thirdTextInput.focus();
                      }}
                      onChangeText={formikProps.handleChange('last_name')}
                      onBlur={formikProps.handleBlur('last_name')}
                      blurOnSubmit={false}
                      //onBlur={() => this._CheckTConditionChacked()}
                      //onFocus={() => this._onFocusChacked()}
                    />
                    {formikProps.touched.last_name &&
                    formikProps.errors.last_name ? (
                      <Text style={RegisterStyles.errMsg}>
                        {formikProps.touched.last_name &&
                          formikProps.errors.last_name}
                      </Text>
                    ) : null}
                    <TextInput
                      ref={(input) => {
                        this.thirdTextInput = input;
                      }}
                      editable={false}
                      style={RegisterStyles.inputStyleBox}
                      placeholderTextColor="#0F2146"
                      placeholder="Email address"
                      //underlineColorAndroid="rgba(0,0,0,0)"
                      keyboardType="email-address"
                      returnKeyType="next"
                      //onChangeText={(text) => this.setState({userName: text})}
                      value={this.state.userInfo.email}
                      autoCorrect={false}
                      disableFullscreenUI={true}
                      autoCapitalize="none"
                      onSubmitEditing={() => {
                        this.fourthTextInput.focus();
                      }}
                      blurOnSubmit={false}
                      //onBlur={() => this._CheckTConditionChacked()}
                      //onFocus={() => this._onFocusChacked()}
                      //onChangeText={formikProps.handleChange('email')}
                      // onBlur={formikProps.handleBlur('email')}
                    />

                    <View
                      style={{
                        backgroundColor: '#F2F5F8',
                        justifyContent: 'center',
                        margin: 20,
                        marginTop: 30,
                      }}>
                      <AnimateLoadingButton
                        ref={(c) => (this.joinNowButton = c)}
                        height={60}
                        width={Dimensions.get('window').width - 40}
                        title="Save"
                        titleFontSize={18}
                        titleColor="rgb(255,255,255)"
                        backgroundColor="#3D73DD"
                        borderRadius={10}
                        titleFontFamily={'NunitoSans-Regular'}
                        onPress={formikProps.handleSubmit}
                        //onPress={this.loginService()}
                        //onPress={this._onPressHandler.bind(this)}
                        //style={{fontFamily:'Arial'}}
                      />
                    </View>

                    <View
                      style={{
                        backgroundColor: '#F2F5F8',
                        justifyContent: 'center',
                        margin: 20,
                        marginTop: 30,
                      }}>
                      <TouchableOpacity
                        style={{
                          height: 60,
                          width: Dimensions.get('window').width - 40,
                          borderRadius: 10,
                          backgroundColor: '#F2F5F8',
                          borderWidth: 1,
                          borderColor: '#D2DAE3',
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                        onPress={() => this.redirectChangePassword()}>
                        <Text
                          style={{
                            color: '#3D73DD',
                            fontSize: 17,
                            fontFamily: 'NunitoSans-Regular',
                          }}>
                          Change password
                        </Text>
                      </TouchableOpacity>

                      {/* <AnimateLoadingButton
                ref={(c) => (this.joinNowButton = c)}
                height={60}
                width={Dimensions.get('window').width - 40}
                title="Join now"
                titleFontSize={18}
                titleColor="rgb(255,255,255)"
                backgroundColor="#3D73DD"
                borderRadius={10}
                
                titleFontFamily={'NunitoSans-Regular'}
                //onPress={formikProps.handleSubmit}
                //onPress={this.loginService()}
                //onPress={this._onPressHandler.bind(this)}
                //style={{fontFamily:'Arial'}}
              /> */}
                    </View>
                  </View>
                )}
              </Formik>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
        {this.state.CustomAlertShow ? (
          <CustomAlert updateAleartParent={this.update_Aleart.bind(this)} />
        ) : null}

        {this.state.showCustomModal ? (
          <CustomModal
            newvalue={this.state.AlertValue}
            updateAleartParent={this.update_Custom_modal.bind(this)}
          />
        ) : null}
      </SafeAreaView>
    );
  }
}
const RegisterStyles = StyleSheet.create({
  inputStyleBox: {
    height: 60,
    margin: 20,
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
    fontSize: 17,
    fontFamily: 'Arial',
    padding: 20,
    color: '#0F2146',
    marginBottom: 0,
    fontFamily: 'NunitoSans-Regular',
  },
  errMsg: {
    color: 'red',

    fontSize: 16,
    letterSpacing: 1,
    marginTop: 5,
    marginLeft: 20,
  },
});
const mapStateToProps = (state) => {
  const {appdata} = state;
  return {appdata};
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateUser: (payload) =>
      dispatch({
        type: 'UPDATE_USER_DETAILS',
        payload: payload,
      }),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
