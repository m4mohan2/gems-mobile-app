import React, {Component} from 'react';

import {
  SafeAreaView,
  View,
  Text,
  PermissionsAndroid,
  Platform,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
  StyleSheet,
  BackHandler,
  ToastAndroid,
  Button,
} from 'react-native';

import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoding';
Geocoder.init('AIzaSyDC7qyMTTAr0YzGKCKMU4vje4uKfD8lKso', {language: 'en'});
import {getDistance, getPreciseDistance} from 'geolib';

class DraggableMarker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentLongitude: this.props.route.params?.lng, //Initial Longitude
      currentLatitude: this.props.route.params?.lat, //Initial Latitude
      count: 0,
    };

    console.log(this.props.route.params?.lat);
    console.log(this.props.route.params?.lat);
  }
  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButton.bind(this),
    );

    var that = this;
    //Checking for the permission just after component loaded
    if (Platform.OS === 'ios') {
      this.callLocation(that);
    } else {
      async function requestLocationPermission() {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
              title: 'Location Access Required',
              message: 'gems needs to Access your location',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            //To Check, If Permission is granted
            that.callLocation(that);
          } else {
            alert('Permission Denied');
          }
        } catch (err) {
          alert('err', err);
          console.warn(err);
        }
      }
      requestLocationPermission();
    }
  }
  componentWillUnmount() {
    Geolocation.clearWatch(this.watchID);
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButton.bind(this),
    );
  }
  handleBackButton() {
    if (this.state.count == 0) {
      this.setState({count: this.state.count + 1});
      ToastAndroid.show('press back again to exist the app', 300);

      setTimeout(() => {
        this.setState({count: 0});
      }, 10000);
      //   let timer = setInterval(this.tick, 10);
      // this.setState({timer});
      return true;
    } else {
      BackHandler.exitApp();
    }
  }
  callLocation(that) {
    Geolocation.getCurrentPosition(
      //Will give you the current location
      (position) => {
        console.log('pos', position);
        const currentLongitude = JSON.stringify(position.coords.longitude);
        //getting the Longitude from the location json
        const currentLatitude = JSON.stringify(position.coords.latitude);
        //getting the Latitude from the location json

        var pdis = getPreciseDistance(
          {
            latitude: this.state.currentLatitude,
            longitude: this.state.currentLongitude,
          },
          {
            latitude: parseFloat(currentLatitude),
            longitude: parseFloat(currentLongitude),
          },
        );

        if (pdis > 200) {
          that.setState({currentLongitude: parseFloat(currentLongitude)});
          //Setting state Longitude to re re-render the Longitude Text
          that.setState({currentLatitude: parseFloat(currentLatitude)});
        }
      },
      (error) => console.log(error),
      {enableHighAccuracy: false, timeout: 20000, maximumAge: 1000},
    );
    that.watchID = Geolocation.watchPosition((position) => {
      if (this.state.updateLocation == true) {
        console.log('hello');
        //Will give you the location on location change
        const currentLongitude = JSON.stringify(position.coords.longitude);
        //getting the Longitude from the location json
        const currentLatitude = JSON.stringify(position.coords.latitude);
        //getting the Latitude from the location json

        var pdis = getPreciseDistance(
          {
            latitude: this.state.currentLatitude,
            longitude: this.state.currentLongitude,
          },
          {
            latitude: parseFloat(currentLatitude),
            longitude: parseFloat(currentLongitude),
          },
        );

        if (pdis > 200) {
          that.setState({currentLongitude: parseFloat(currentLongitude)});
          //Setting state Longitude to re re-render the Longitude Text
          that.setState({currentLatitude: parseFloat(currentLatitude)});
        }
        // that.setState({currentLongitude: parseFloat(currentLongitude)});
        // //Setting state Longitude to re re-render the Longitude Text
        // that.setState({currentLatitude: parseFloat(currentLatitude)});
      }
      //Setting state Latitude to re re-render the Longitude Text
    });
  }
  afterDragEnd = (val) => {
    console.log(val);
    Geocoder.from(val)
      .then((json) => {
        console.log(JSON.stringify(json));
        var addressComponent = json.results[0].address_components[0];
        console.log(addressComponent);
        this.props.navigation.navigate('GemAddForm', {
          address: json.results[0].formatted_address,
          location: json.results[0].geometry.location,
          place_id: json.results[0].place_id,
          draggable: true,
        });
      })
      .catch((error) => console.warn(error));
  };
  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#222529'}}>
        <View style={{flex: 1}}>
          {this.state.currentLatitude != null ? (
            <MapView
              style={{flex: 1}}
              provider={PROVIDER_GOOGLE}
              region={{
                latitude: this.state.currentLatitude,
                longitude: this.state.currentLongitude,
                latitudeDelta: 0.02,
                longitudeDelta: 0.01,
              }}
              mapType="standard"
              zoomEnabled={false}
              zoomControlEnabled={false}
              zoomTapEnabled={false}
              // showsUserLocation={true}
              showsBuildings={true}
              showsIndoors={true}
              showsIndoorLevelPicker={true}>
              <Marker
                ref={(ref) => {
                  this.markerView = ref;
                }}
                coordinate={{
                  latitude: parseFloat(this.state.currentLatitude),
                  longitude: parseFloat(this.state.currentLongitude),
                }}
                title="Drag and Drop Gem to add"
                tracksViewChanges={false}
                draggable
                onDragEnd={(e) => this.afterDragEnd(e.nativeEvent.coordinate)}>
                <Image
                  source={require('../Images/markerBlue.png')}
                  style={{height: 80, width: 58}}
                />
              </Marker>
            </MapView>
          ) : null}

          <TouchableOpacity
            style={{
              height: 40,
              width: 40,
              position: 'absolute',
              marginTop: 30,
              right: 20,
            }}
            onPress={() => this.props.navigation.navigate('AddGems')}>
            <View
              style={{
                position: 'absolute',
                marginTop: 0,
                width: 2,
                height: 16,
                backgroundColor: '#222529',
                right: 10,
                transform: [{rotate: '45deg'}],
              }}></View>
            <View
              style={{
                position: 'absolute',
                marginTop: 0,
                width: 2,
                height: 16,
                backgroundColor: '#222529',
                right: 10,
                transform: [{rotate: '-45deg'}],
              }}></View>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

export default DraggableMarker;
