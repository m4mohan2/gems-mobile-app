import React, {Component} from 'react';

import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  useEffect,
  BackHandler,
  ToastAndroid,
} from 'react-native';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import Loader from './Loader';
import {TouchableNativeFeedback} from 'react-native-gesture-handler';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {profileInfo: {}, isLoading: true, count: 0};

    console.log('before redux uopdate', this.props);
  }

  componentDidMount() {
    //
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButton.bind(this),
    );
    this.fetchProfile();
  }
  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButton.bind(this),
    );
  }
  handleBackButton() {
    if (this.state.count == 0) {
      this.setState({count: this.state.count + 1});
      ToastAndroid.show('press back again to exist the app', 300);

      setTimeout(() => {
        this.setState({count: 0});
      }, 10000);
      //   let timer = setInterval(this.tick, 10);
      // this.setState({timer});
      return true;
    } else {
      BackHandler.exitApp();
    }
  }
  fetchProfile = () => {
    this.setState({isLoading: true});
    AsyncStorage.getItem('userInfo').then(
      (value) => {
        const jsonValue = JSON.parse(value);
        console.log('======', jsonValue.token);
        console.log('======', jsonValue.user.id);
        fetch(this.props.appdata.RELEASE_API_URL + 'api/profileDetails', {
          method: 'POST',
          headers: {
            token: 'Bearer ' + jsonValue.token,
          },
          body: JSON.stringify({
            user_id: jsonValue.user.id,
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
            console.log('++++', responseJson);
            jsonValue.user = responseJson.user;
            this.props.updateUser(responseJson.user);

            this.setState(
              {profileInfo: responseJson.user, isLoading: false},
              () => console.log('after redux update', this.props),
            );

            AsyncStorage.setItem('userInfo', JSON.stringify(jsonValue));
          })
          .catch((error) => {
            console.log(error);
          });
      },
      //AsyncStorage returns a promise so adding a callback to get the value

      //Setting the value in Text
    );
  };

  goEditProfile = () => {
    this.props.navigation.navigate('EditProfile', {
      userInfo: this.state.profileInfo,
    });
  };

  logoutFunction = () => {
    AsyncStorage.clear();
    this.props.navigation.navigate('OnBoarding_3');
  };

  // refresh() {
  //   AsyncStorage.getItem('userInfo').then(
  //     (value) => {
  //       console.log('nnnn', value);
  //       this.setState({profileInfo: value.user});
  //     },
  //     //AsyncStorage returns a promise so adding a callback to get the value

  //     //Setting the value in Text
  //   );
  // }

  render() {
    //console.log('email', this.state.user.email)
    if (this.state.isLoading == false) {
      console.log(
        'gems',
        this.props.appdata.USER_DETAILS.user_detail.first_name,
      );
    }

    return (
      <SafeAreaView style={{flex: 1,backgroundColor: '#222529'}}>
        {this.state.isLoading == true ? (
          <Loader />
        ) : (
          <View style={{flex: 1, backgroundColor: '#222529'}}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <TouchableOpacity
                style={{
                  marginLeft: 20,
                  height: 20,
                  width: 30,
                  marginTop: 41,
                }}
                onPress={() => this.props.navigation.goBack()}>
                <Image
                  style={{height: 20, width: 20}}
                  source={require('../Images/back.png')}
                  resizeMode="contain"></Image>
              </TouchableOpacity>
              <Text
                style={{
                  textAlign: 'center',
                  color: 'white',
                  fontSize: 32,
                  marginTop: 18,
                  alignSelf: 'center',
                  fontFamily: 'NunitoSans-ExtraBold',
                }}>
                gems
              </Text>
              <TouchableOpacity
                style={{
                  height: 50,
                  marginTop: 25,
                  marginRight: 10,
                  width: 60,
                  alignItems: 'center',
                  //backgroundColor: 'red',
                }}>
                <Image
                  style={{
                    width: 31,
                    height: 31,
                    marginTop: 10,
                  }}
                  source={require('../Images/Profile_new.png')}
                  resizeMode="contain"
                  resizeMethod="scale"></Image>
              </TouchableOpacity>
              <View
                style={{
                  position: 'absolute',
                  height: 6,
                  width: 10,
                  backgroundColor: '#F2F5F8',
                  top: 84,
                  right: 35,
                  borderTopLeftRadius: 5,
                  borderTopRightRadius: 5,
                }}></View>
            </View>

            <View
              style={{
                flex: 1,
                backgroundColor: '#F2F5F8',
                marginTop: 14,
                borderTopLeftRadius: 30,
                borderTopRightRadius: 30,
              }}>
              <TouchableOpacity
                style={{
                  height: 30,
                  width: 30,
                  marginTop: 20,
                  marginLeft: Dimensions.get('window').width - 60,
                  alignItems: 'center',
                }}>
                <Image
                  style={{
                    width: 25,
                    height: 25,
                  }}
                  source={require('../Images/Settings.png')}
                  resizeMode="contain"
                  resizeMethod="scale"></Image>
              </TouchableOpacity>
              {this.state.isLoading == false ? (
                <View style={{flexDirection: 'row'}}>
                  <Image
                    style={{
                      width: 70,
                      height: 70,
                      marginLeft: 20,
                      borderRadius: 70 / 2,
                    }}
                    source={
                      this.props.appdata.USER_DETAILS.user_detail
                        .profile_pic !== null
                        ? {
                            uri:
                              this.props.appdata.IMAGE_PREFIX_URL +
                              this.props.appdata.USER_DETAILS.user_detail
                                .profile_pic,
                          }
                        : require('../Images/defaultProfile.png')
                    }
                    //source={require('../Images/Settings.png')}
                    //resizeMode="contain"
                  ></Image>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'column',
                      //backgroundColor: 'grey',
                    }}>
                    <Text
                      style={{
                        marginLeft: 10,
                        color: '#0F2146',
                        fontSize: 17,
                        fontFamily: 'NunitoSans-Bold',
                      }}>
                      {this.props.appdata.USER_DETAILS.user_detail.first_name +
                        ' ' +
                        this.props.appdata.USER_DETAILS.user_detail.last_name}
                    </Text>

                    <Text
                      style={{
                        marginLeft: 10,
                        color: '#0F2146',
                        fontSize: 17,
                        fontFamily: 'NunitoSans-Regular',
                      }}>
                      {this.props.appdata.USER_DETAILS.email}
                    </Text>
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        style={{
                          width: 70,
                          height: 17,
                          backgroundColor: '#68B3FF',
                          borderRadius: 2,
                          marginLeft: 10,
                          marginTop: 5,
                        }}>
                        <Text
                          style={{
                            color: '#FFFFFF',
                            fontSize: 10,
                            fontFamily: 'NunitoSans-Regular',
                            textAlign: 'center',
                          }}
                          onPress={this.goEditProfile}>
                          Edit profile
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={{
                          width: 70,
                          height: 17,
                          backgroundColor: '#68B3FF',
                          borderRadius: 2,
                          marginLeft: 10,
                          marginTop: 5,
                        }}>
                        <Text
                          style={{
                            color: '#FFFFFF',
                            fontSize: 10,
                            fontFamily: 'NunitoSans-Regular',
                            textAlign: 'center',
                          }}
                          onPress={this.logoutFunction}>
                          Logout
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              ) : null}
            </View>
          </View>
        )}
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  const {appdata} = state;
  return {appdata};
};
const mapDispatchToProps = (dispatch) => {
  return {
    updateUser: (payload) =>
      dispatch({
        type: 'UPDATE_USER_DETAILS',
        payload: payload,
      }),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Profile);
