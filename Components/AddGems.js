import React, {Component} from 'react';

import {
  SafeAreaView,
  View,
  Text,
  PermissionsAndroid,
  Platform,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
  StyleSheet,
  BackHandler,
  ToastAndroid,
} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
const GOOGLE_MAPS_APIKEY = 'AIzaSyD_BBiV2jnDTjEMxjvFncobeQ3RY0wUP-Y';
import Geolocation from '@react-native-community/geolocation';
import {getDistance, getPreciseDistance} from 'geolib';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import MarkerInfo from './MarkerInfoModal';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';

const mapStyle = [
  {
    featureType: 'poi',
    stylers: [
      {
        visibility: 'off',
      },
    ],
  },
];

class AddGem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentLongitude: null, //Initial Longitude
      currentLatitude: null, //Initial Latitude
      plot: [],
      updateLocation: true,
      markerInfoShow: false,
      selectedLocation: null,
      gemList: [],
      userLoactionLat: null,
      userLocationLng: null,
      user_id: 0,
      count: 0,
    };
  }

  getGems = () => {
    console.log('i am called');
    AsyncStorage.getItem('userInfo')
      .then(
        (value) => {
          const jsonValue = JSON.parse(value);
          console.log('======', jsonValue.token);
          console.log('======', jsonValue.user.id);
          this.setState({user_id: jsonValue.user.id});

          fetch(this.props.appdata.RELEASE_API_URL + 'api/listGem', {
            method: 'GET',
            headers: {
              token: 'Bearer ' + jsonValue.token,
            },
          })
            .then((response) => response.json())
            .then((responseJson) => {
              console.log('gemList', responseJson);
              this.setState({gemList: responseJson});
            })
            .catch((error) => {
              console.log(error);
              alert(error);
            });
        },
        //AsyncStorage returns a promise so adding a callback to get the value

        //Setting the value in Text
      )
      .catch((error) => {
        //alert(error);
      });
  };

  goProfile = () => {
    this.props.navigation.navigate('Profile', {
      refresh: false,
    });
  };
  cancelSearch = () => {
    this.setState(
      {
        updateLocation: true,
        currentLatitude: this.state.userLoactionLat,
        currentLongitude: this.state.userLocationLng,
      },
      () => this.getNearByPlace(),
    );
  };

  getNearByPlace = () => {
    const url =
      'https://maps.googleapis.com/maps/api/place/nearbysearch/json?' +
      'location=' +
      this.state.currentLatitude +
      ',' +
      this.state.currentLongitude +
      '&radius=500&type=restaurant&key=AIzaSyDC7qyMTTAr0YzGKCKMU4vje4uKfD8lKso';
    fetch(url)
      .then((response) => response.json())
      .then((JsonResponse) => {
        if (JsonResponse.status == 'OK') {
          this.setState({plot: JsonResponse.results});
        }
      })
      .catch((error) => {
        // alert('error');
      });
  };
  componentDidUpdate() {}
  componentWillReceiveProps(nextProps) {
    if (nextProps.route.params?.selection !== undefined) {
      this.setState({
        currentLatitude: nextProps.route.params?.selection.current_lat,
        currentLongitude: nextProps.route.params?.selection.current_lng,
        plot: [nextProps.route.params?.selection.plot],
        updateLocation: false,
      });
    }

    if (nextProps.route.params?.draggable == true) {
      this.getGems();
    }
  }
  componentDidMount = () => {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButton.bind(this),
    );
    this.getGems();
    if (Platform.OS === 'ios') {
      this.getUserCurrentLocation();
    } else {
      RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
        interval: 10000,
        fastInterval: 5000,
      })
        .then((data) => {
          this.getUserCurrentLocation();
        })
        .catch((err) => {
          // The user has not accepted to enable the location services or something went wrong during the process
          // "err" : { "code" : "ERR00|ERR01|ERR02", "message" : "message"}
          // codes :
          //  - ERR00 : The user has clicked on Cancel button in the popup
          //  - ERR01 : If the Settings change are unavailable
          //  - ERR02 : If the popup has failed to open
        });
    }
  };
  callLocation(that) {
    if (this.state.updateLocation == true) {
      Geolocation.getCurrentPosition(
        //Will give you the current location
        (position) => {
          console.log('pos', position);
          const currentLongitude = JSON.stringify(position.coords.longitude);
          //getting the Longitude from the location json
          const currentLatitude = JSON.stringify(position.coords.latitude);
          //getting the Latitude from the location json

          that.setState({userLocationLng: parseFloat(currentLongitude)});
          //Setting state Longitude to re re-render the Longitude Text
          that.setState({userLoactionLat: parseFloat(currentLatitude)});

          that.setState({currentLongitude: parseFloat(currentLongitude)});
          //Setting state Longitude to re re-render the Longitude Text
          that.setState({currentLatitude: parseFloat(currentLatitude)}, () =>
            this.getNearByPlace(),
          );
        },
        (error) => console.log(error),
        {enableHighAccuracy: false, timeout: 40000, maximumAge: 36000},
      );
    }
    that.watchID = Geolocation.watchPosition((position) => {
      if (this.state.updateLocation == true) {
        console.log('hello');
        //Will give you the location on location change
        const currentLongitude = JSON.stringify(position.coords.longitude);
        //getting the Longitude from the location json
        const currentLatitude = JSON.stringify(position.coords.latitude);
        //getting the Latitude from the location json

        that.setState({userLocationLng: parseFloat(currentLongitude)});
        //Setting state Longitude to re re-render the Longitude Text
        that.setState({userLoactionLat: parseFloat(currentLatitude)});

        that.setState({currentLongitude: parseFloat(currentLongitude)});
        //Setting state Longitude to re re-render the Longitude Text
        that.setState({currentLatitude: parseFloat(currentLatitude)}, () =>
          this.getNearByPlace(),
        );
      }
      //Setting state Latitude to re re-render the Longitude Text
    });
  }
  getUserCurrentLocation = () => {
    var that = this;
    //Checking for the permission just after component loaded
    if (Platform.OS === 'ios') {
      this.callLocation(that);
    } else {
      async function requestLocationPermission() {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
              title: 'Location Access Required',
              message: 'gems needs to Access your location',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            //To Check, If Permission is granted

            that.callLocation(that);
          } else {
            // alert('Permission Denied');
          }
        } catch (err) {
          //alert('err', err);
          console.warn(err);
        }
      }
      requestLocationPermission();
    }
  };

  componentWillUnmount = () => {
    Geolocation.clearWatch(this.watchID);
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButton.bind(this),
    );
  };

  handleBackButton() {
    if (this.state.count == 0) {
      this.setState({count: this.state.count + 1});
      ToastAndroid.show('press back again to exist the app', 300);

      setTimeout(() => {
        this.setState({count: 0});
      }, 10000);
      //   let timer = setInterval(this.tick, 10);
      // this.setState({timer});
      return true;
    } else {
      BackHandler.exitApp();
    }
  }
  markerClicked = (markerVal) => {
    console.log('marker Clicked');

    console.log(JSON.stringify(markerVal));
    this.setState({markerInfoShow: true, selectedLocation: markerVal});
  };

  onRegionChange(region) {
    console.log('region change fire');

    var pdis = getPreciseDistance(
      {
        latitude: this.state.currentLatitude,
        longitude: this.state.currentLongitude,
      },
      {
        latitude: parseFloat(region.latitude),
        longitude: parseFloat(region.longitude),
      },
    );

    var previousValue = 0;
    if (pdis - previousValue > 1000) {
      previousValue = pdis;
      this.setState({currentLongitude: parseFloat(region.longitude)});
      //Setting state Longitude to re re-render the Longitude Text
      this.setState({currentLatitude: parseFloat(region.latitude)}, () =>
        this.getNearByPlace(),
      );
    }
    console.log(pdis);
    // this.setState({currentLongitude: parseFloat(region.latitude)});
    // //Setting state Longitude to re re-render the Longitude Text
    // this.setState({currentLatitude: parseFloat(region.longitude)}, () =>
    //   this.getNearByPlace(),
    // );
  }
  Item({title}) {
    return (
      <View style={styles.item}>
        <Text style={styles.title}>{title}</Text>
      </View>
    );
  }
  update_Custom_modal = (val) => {
    if (val == 1) {
      this.setState({markerInfoShow: false});
    } else {
      var pdis = getPreciseDistance(
        {
          latitude: this.state.userLoactionLat,
          longitude: this.state.userLocationLng,
        },
        {
          latitude: parseFloat(
            this.state.selectedLocation.geometry.location.lat,
          ),
          longitude: parseFloat(
            this.state.selectedLocation.geometry.location.lng,
          ),
        },
      );
      this.setState({markerInfoShow: false});
      this.props.navigation.navigate('GemAddForm', {
        gemInfo: this.state.selectedLocation,
        distance: pdis > 1000 ? (pdis / 1000).toFixed(2) + ' km' : pdis + ' m',
        refresh: this.backRefresh.bind(this),
      });
    }
  };

  backRefresh = () => {
    this.getGems();
    //this.getUserCurrentLocation();
  };
  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#191C1F'}}>
        <View
          style={{
            flex: 1,
            // alignItems: 'center',
            // justifyContent: 'center',
            backgroundColor: '#F2F5F8',

            borderBottomRightRadius: 30,
            borderBottomLeftRadius: 30,
            overflow: 'hidden',
          }}>
          {this.state.currentLatitude != null ? (
            <MapView
              style={{flex: 1}}
              provider={PROVIDER_GOOGLE}
              loadingEnabled={true}
              region={{
                latitude: this.state.currentLatitude,
                longitude: this.state.currentLongitude,
                latitudeDelta: 0.02,
                longitudeDelta: 0.01,
              }}
              mapType="standard"
              showsPointsOfInterest={false}
              showsBuildings={false}
              showsIndoors={false}
              showsIndoorLevelPicker={false}
              customMapStyle={mapStyle}
              onRegionChangeComplete={this.onRegionChange.bind(this)}>
              {this.state.plot.length > 0 &&
                this.state.plot.map((marker, index) => {
                  console.log(marker.name);
                  console.log('markerLocation', marker.place_id);

                  console.log(
                    this.state.gemList.some(
                      (item) => marker.place_id === item.place_id,
                    ),
                  );
                  return this.state.gemList.some(
                    (item) => marker.place_id === item.place_id,
                  ) ? null : (
                    <Marker
                      key={index}
                      ref={(ref) => {
                        this.markerView = ref;
                      }}
                      coordinate={{
                        latitude: parseFloat(marker.geometry.location.lat),
                        longitude: parseFloat(marker.geometry.location.lng),
                      }}
                      //title={marker.name}
                      onPress={() => this.markerClicked(marker)}>
                      <Image
                        source={require('../Images/redMarker.png')}
                        style={{height: 35, width: 26}}
                      />
                    </Marker>
                  );

                  //});
                })}
              {this.state.gemList.map((gem, index) => {
                console.log('===>', this.state.user_id);
                console.log('++++++', gem.created_by.id);
                return (
                  <Marker
                    // pinColor={gem.created_by.id == this.state ? 'green' : 'blue'}
                    key={index}
                    ref={(ref) => {
                      this.markerView = ref;
                    }}
                    coordinate={{
                      latitude: parseFloat(gem.lat),
                      longitude: parseFloat(gem.lng),
                    }}
                    //title={marker.name}
                  >
                    <Image
                      source={
                        gem.created_by.id == this.state.user_id
                          ? require('../Images/markerBlue.png')
                          : require('../Images/markerGreen.png')
                      }
                      style={{height: 35, width: 26}}
                    />
                  </Marker>
                );
              })}
            </MapView>
          ) : null}
          <View
            style={{
              height: 55,
              margin: 10,
              width: '95%',
              marginTop: 20,
              borderRadius: 10,
              backgroundColor: '#FFFFFF',
              flexDirection: 'row-reverse',
              position: 'absolute',
            }}>
            <TouchableOpacity
              style={{
                height: 55,
                marginTop: 0,
                marginEnd: 0,
                width: 60,
                //backgroundColor: 'red',
              }}
              onPress={
                this.state.updateLocation == false
                  ? this.cancelSearch
                  : this.goProfile
              }>
              {this.state.updateLocation == false ? (
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: 10,
                  }}>
                  <View
                    style={{
                      position: 'absolute',
                      marginTop: 0,
                      width: 2,
                      height: 15,
                      backgroundColor: '#222529',
                      right: 10,
                      transform: [{rotate: '45deg'}],
                    }}></View>
                  <View
                    style={{
                      position: 'absolute',
                      marginTop: 0,
                      width: 2,
                      height: 15,
                      backgroundColor: '#222529',
                      right: 10,
                      transform: [{rotate: '-45deg'}],
                    }}></View>
                </View>
              ) : (
                <Image
                  style={{
                    width: 40,
                    height: 37,
                    marginTop: 10,
                  }}
                  source={require('../Images/Profile.png')}
                  resizeMode="contain"
                  resizeMethod="scale"></Image>
              )}
            </TouchableOpacity>
            {/* <View
              style={{
                width: 1,
                height: 35,
                backgroundColor: '#CCD2D9',
                margin: 10,
              }}></View> */}
            <TextInput
              style={{
                left: 0,
                marginTop: 0,
                marginRight: 10,
                marginBottom: 0,
                marginLeft: 0,
                flex: 1,
                paddingRight: 10,
                paddingLeft: 20,
                fontSize: 17,
                fontFamily: 'NunitoSans-Regular',
              }}
              placeholder="Add Gem"
              placeholderTextColor="#0F2146"
              autoFocus={false}
              value={
                this.state.updateLocation == false
                  ? this.state.plot[0].name
                  : ''
              }
              onFocus={() =>
                this.props.navigation.navigate('GooglePlaceSearch', {
                  lat: this.state.currentLatitude,
                  lng: this.state.currentLongitude,
                })
              }></TextInput>
          </View>
        </View>
        {this.state.markerInfoShow ? (
          <MarkerInfo
            newvalue={this.state.selectedLocation}
            updateAleartParent={this.update_Custom_modal.bind(this)}
          />
        ) : null}
      </SafeAreaView>
    );
  }
}
const RegisterStyles = StyleSheet.create({
  inputStyleBox: {
    height: 60,
    margin: 20,
    backgroundColor: '#F2F5F8',
    borderRadius: 5,
    fontSize: 17,
    fontFamily: 'NunitoSans-Regular',
    padding: 20,
    color: '#0F2146',
    marginBottom: 0,
  },
});

const mapStateToProps = (state) => {
  const {appdata} = state;
  return {appdata};
};
export default connect(mapStateToProps)(AddGem);
