import React, {Component} from 'react';
import {
  SafeAreaView,
  Image,
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  Dimensions,
  KeyboardAvoidingView,
} from 'react-native';
import AnimateLoadingButton from 'react-native-animate-loading-button';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';


const initialValues = {
  email: '',
  password: '',
  message: '',
};
const validationLogin = Yup.object().shape({
  email: Yup.string()
    .required('The email field is required.')
    .email('The email must be a valid email address.'),
  password: Yup.string().required('The password field is required.'),
});
class Login extends Component {
  redirectOnboard = () => {
    this.props.navigation.navigate('OnBoarding_3');
  };
  render() {
    console.warn('this.props Login ===>>', this.props);
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#222529'}}>
        <KeyboardAvoidingView
          style={{flex: 1}}
          behavior={Platform.OS == 'ios' ? 'padding' : 'height'}>
          {/* <ScrollView style={{flex: 1}}> */}
          <Text
            style={{
              position: 'absolute',
              textAlign: 'center',
              color: 'white',
              fontSize: 32,
              marginTop: 30,
              //justifyContent: 'flex-start',
              alignSelf: 'center',
              fontFamily: 'NunitoSans-ExtraBold',
            }}>
            gems
          </Text>
          <View
            style={{
              flex: 1,
              justifyContent: 'center', // Used to set Text Component Vertically Center
              alignItems: 'center',
              flexDirection: 'column',
            }}>
            <Formik
              initialValues={initialValues}
              onSubmit={(values, actions) => {
                this.loadingButton.showLoading(true);

                fetch(this.props.appdata.RELEASE_API_URL + 'api/login', {
                  method: 'POST',
                  headers: {
                    'Content-Type': 'application/json',
                  },

                  body: JSON.stringify({
                    email: values.email,
                    password: values.password,
                  }),
                })
                  .then((response) => response.json())
                  .then((responseJson) => {
                    this.loadingButton.showLoading(false);
                    if (responseJson.success == true) {
                      const jsonValue = JSON.stringify(responseJson)
                      AsyncStorage.setItem('userInfo', jsonValue);
                      this.props.updateUserToken(responseJson.token);
                      this.props.navigation.navigate('DashBoard', {
                        first_name: responseJson.user.user_detail.first_name,
                      });
                    } else {
                      actions.setErrors(responseJson);
                    }
                  })
                  .catch((error) => {
                    console.log(error);
                    alert('Please Try Again!');
                    this.loadingButton.showLoading(false);
                  });
              }}
              validationSchema={validationLogin}>
              {(formikProps) => (
                <View
                  style={{
                    alignSelf: 'center',

                    width: '90%',
                    backgroundColor: '#FFFFFF',
                    borderRadius: 10,
                    //marginTop: 100,
                  }}>
                  <TouchableOpacity
                    style={{
                      height: 40,
                      width: 40,
                      position: 'absolute',
                      marginTop: 30,
                      right: 20,
                      //backgroundColor: 'red',
                    }}
                    onPress={this.redirectOnboard}>
                    <View
                      style={{
                        position: 'absolute',
                        marginTop: 0,
                        width: 2,
                        height: 10,
                        backgroundColor: '#222529',
                        right: 10,
                        transform: [{rotate: '45deg'}],
                      }}></View>
                    <View
                      style={{
                        position: 'absolute',
                        marginTop: 0,
                        width: 2,
                        height: 10,
                        backgroundColor: '#222529',
                        right: 10,
                        transform: [{rotate: '-45deg'}],
                      }}></View>
                  </TouchableOpacity>
                  <Text
                    style={{
                      alignSelf: 'center',
                      marginTop: 30,
                      fontSize: 30,
                      color: '#0F2146',
                      marginBottom: 20,
                      fontFamily: 'NunitoSans-Bold',
                    }}>
                    Login
                  </Text>
                  {formikProps.errors.message ? (
                    <Text style={RegisterStyles.errMsg}>
                      {formikProps.errors.message}
                    </Text>
                  ) : null}

                  <TextInput
                    ref={(input) => {
                      this.firstTextInput = input;
                    }}
                    style={RegisterStyles.inputStyleBox}
                    placeholderTextColor="#0F2146"
                    placeholder="Email"
                    secureTextEntry={false}
                    //underlineColorAndroid="rgba(0,0,0,0)"
                    keyboardType="default"
                    returnKeyType="next"
                    //onChangeText={(text) => this.setState({userName: text})}
                    //value={this.state.userName}
                    autoCorrect={false}
                    disableFullscreenUI={true}
                    autoCapitalize="none"
                    onSubmitEditing={() => {
                      this.secondTextInput.focus();
                    }}
                    blurOnSubmit={true}
                    //onBlur={() => this._CheckTConditionChacked()}
                    //onFocus={() => this._onFocusChacked()}
                    onChangeText={formikProps.handleChange('email')}
                    onBlur={formikProps.handleBlur('email')}
                  />
                  {formikProps.touched.email && formikProps.errors.email ? (
                    <Text style={RegisterStyles.errMsg}>
                      {formikProps.touched.email && formikProps.errors.email}
                    </Text>
                  ) : null}
                  <TextInput
                    ref={(input) => {
                      this.secondTextInput = input;
                    }}
                    style={RegisterStyles.inputStyleBox}
                    placeholderTextColor="#0F2146"
                    placeholder="Password"
                    secureTextEntry={true}
                    //underlineColorAndroid="rgba(0,0,0,0)"
                    keyboardType="default"
                    returnKeyType="done"
                    //onChangeText={(text) => this.setState({userName: text})}
                    //value={this.state.userName}
                    autoCorrect={false}
                    disableFullscreenUI={true}
                    autoCapitalize="none"
                    //onSubmitEditing={() => { this.secondTextInput.focus(); }}
                    blurOnSubmit={true}
                    //onBlur={() => this._CheckTConditionChacked()}
                    //onFocus={() => this._onFocusChacked()}
                    onChangeText={formikProps.handleChange('password')}
                    onBlur={formikProps.handleBlur('password')}
                  />
                  {formikProps.touched.password &&
                  formikProps.errors.password ? (
                    <Text style={RegisterStyles.errMsg}>
                      {formikProps.touched.password &&
                        formikProps.errors.password}
                    </Text>
                  ) : null}
                  <View
                    style={{
                      //flex: 1,
                      backgroundColor: 'rgb(255,255,255)',
                      justifyContent: 'center',
                      margin: 20,
                      marginTop: 30,
                    }}>
                    <AnimateLoadingButton
                      ref={(c) => (this.loadingButton = c)}
                      height={60}
                      width={Dimensions.get('window').width - 70}
                      title="Sign in"
                      titleFontSize={18}
                      titleColor="rgb(255,255,255)"
                      backgroundColor="#3D73DD"
                      borderRadius={10}
                      titleFontFamily={'NunitoSans-Regular'}
                      onPress={formikProps.handleSubmit}
                      //onPress={this.loginService()}
                      //onPress={this._onPressHandler.bind(this)}
                      //style={{fontFamily:'Arial'}}
                    />
                  </View>
                  <Text
                    style={{
                      marginTop: 10,
                      marginBottom: 40,
                      fontSize: 14,
                      color: '#3D73DD',
                      alignSelf: 'center',
                      textAlign: 'center',
                      fontFamily: 'NunitoSans-Regular',
                    }} onPress = {() => this.props.navigation.navigate('ForgetPassword') }>
                    Forgot your password?
                  </Text>
                </View>
              )}
            </Formik>
          </View>
          {/* </ScrollView> */}
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}
const RegisterStyles = StyleSheet.create({
  inputStyleBox: {
    height: 60,
    margin: 20,
    backgroundColor: '#F2F5F8',
    borderRadius: 5,
    fontSize: 17,
    fontFamily: 'Arial',
    padding: 20,
    color: '#0F2146',
    marginBottom: 0,
  },
  errMsg: {
    color: 'red',

    fontSize: 17,
    letterSpacing: 1,
    marginTop: 5,
    marginLeft: 20,
    fontFamily: 'NunitoSans-Regular',
  },
});

const mapStateToProps = (state) => {
  const {appdata} = state;
  return {appdata};
};

function mapDispatchToProps(dispatch) {
  return {
    updateUserToken: (payload) =>
      dispatch({
        type: 'USER_TOKEN_UPDATE',
        payload: payload,
      }),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);
