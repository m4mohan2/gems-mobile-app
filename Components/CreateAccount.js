import React, {Component} from 'react';
import {
  SafeAreaView,
  Image,
  ScrollView,
  View,
  Text,
  TextInput,
  StyleSheet,
  KeyboardAvoidingView,
  Dimensions,
  Alert,
} from 'react-native';
import AnimateLoadingButton from 'react-native-animate-loading-button';
import {Formik} from 'formik';
import * as Yup from 'yup';
const FBSDK = require('react-native-fbsdk');
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';


const {LoginManager, AccessToken, GraphRequest, GraphRequestManager} = FBSDK;
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';
const initialValues = {
  first_name: '',
  last_name: '',
  email: '',
  password: '',
};

const validationRegister = Yup.object().shape({
  first_name: Yup.string().required('The first name field is required.'),
  last_name: Yup.string().required('The last name field is required.'),
  email: Yup.string()
    .required('The email field is required.')
    .email('The email must be a valid email address.'),
  password: Yup.string().required('The password field is required.'),
});

class CreateAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {userInfo: {}};
  }

  customAlert = (title, msg) =>
    Alert.alert(
      title,
      msg,
      [{text: 'OK', onPress: () => console.log('OK Pressed')}],
      {cancelable: false},
    );

  componentDidMount() {
    GoogleSignin.configure({
      webClientId:
        '257079715413-e69pllnnjiqdtta0lc4tvkf0t763ugeg.apps.googleusercontent.com', // client ID of type WEB for your server(needed to verify user ID and offline access)
      offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
    });

    //Check if user is already signed in
    this._isSignedIn();
  }

  _isSignedIn = async () => {
    const isSignedIn = await GoogleSignin.isSignedIn();
    if (isSignedIn) {
      //Get the User details as user is already signed in
      this._getCurrentUserInfo();
    } else {
      //alert("Please Login");
    }
    this.setState({gettingLoginStatus: false});
  };

  _getCurrentUserInfo = async () => {
    try {
      const userInfo = await GoogleSignin.signInSilently();
      console.log('User Info --> ', userInfo);
      this.setState({userInfo: userInfo});
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_REQUIRED) {
        console.log('User has not signed in yet');
      } else {
        alert("Something went wrong. Unable to get user's info");
        console.log("Something went wrong. Unable to get user's info");
      }
    }
  };

  _signIn = async () => {
    //Prompts a modal to let the user sign in into your application.

    console.log('Google Login');
    try {
      await GoogleSignin.hasPlayServices({
        //Check if device has Google Play Services installed.
        //Always resolves to true on iOS.
        showPlayServicesUpdateDialog: true,
      });
      const userInfo = await GoogleSignin.signIn();
      console.log('User Info --> ', userInfo);
      this.setState({userInfo: userInfo}, () => this.registerAPICall('Google'));
    } catch (error) {
      console.log('Message', error.message);

      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('User Cancelled the Login Flow');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('Signing In');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('Play Services Not Available or Outdated');
      } else {
        console.log('Some Other Error Happened');
      }
    }
  };

  _onPressHandler() {
    console.log('hello');
  }

  loginFacebook() {
    //this.facebookButton.showLoading(true);

    // if (Platform.OS === 'android') {
    //   LoginManager.setLoginBehavior('web_only');
    // }
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
      (login) => {
        if (login.isCancelled) {
          console.log('Login Cancel');
          {
            this.customAlert('Facebook', 'Login cancel');
          }
          this.facebookButton.showLoading(false);
        } else {
          AccessToken.getCurrentAccessToken().then((data) => {
            const accessToken = data.accessToken.toString();

            this.getInfofromToken(accessToken);
          });
        }
      },
      (error) => {
        this.facebookButton.showLoading(false);
        console.log('error', error);
      },
    );
  }
  getInfofromToken = (token) => {
    const PROFILE_REQUEST_PARAMS = {
      fields: {
        string: 'email, id, name, first_name, last_name',
      },
    };
    const profileRequest = new GraphRequest(
      '/me',
      {
        token,
        parameters: PROFILE_REQUEST_PARAMS,
      },
      (error, result) => {
        if (error) {
        } else {
          this.setState({userInfo: result}, () =>
            this.registerAPICall('Facebook'),
          );
        }
      },
    );
    new GraphRequestManager().addRequest(profileRequest).start();
  };
  redirectLogin() {
    this.props.navigation.navigate('Login');
  }

  registerAPICall(socialMedia) {
    fetch(this.props.appdata.RELEASE_API_URL + 'api/socialMediaLogin', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },

      body: JSON.stringify({
        email:
          socialMedia == 'Facebook'
            ? this.state.userInfo.email
            : this.state.userInfo.user.email,

        provider_user_id:
          socialMedia == 'Facebook'
            ? this.state.userInfo.id
            : this.state.userInfo.user.id,
        provider: socialMedia == 'Facebook' ? 'Facebook' : 'Google',
        first_name:
          socialMedia == 'Facebook'
            ? this.state.userInfo.first_name
            : this.state.userInfo.user.givenName,
        last_name:
          socialMedia == 'Facebook'
            ? this.state.userInfo.last_name
            : this.state.userInfo.user.familyName,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.success == true) {
          console.log(responseJson);
          const jsonValue = JSON.stringify(responseJson);
          AsyncStorage.setItem('userInfo', jsonValue);
         // this.props.updateUserToken(responseJson.token);
          this.props.navigation.navigate('DashBoard', {
            first_name: responseJson.user.user_detail.first_name,
          });
        } else {
          this.facebookButton.showLoading(false);
          this.googleButton.showLoading(false);
          alert('Please try again!');
        }
      })
      .catch((error) => {
        alert(error);
      });
  }

  render() {
    console.warn('this.props ===>>', this.props);
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#222529'}}>
        <KeyboardAvoidingView
          style={{flex: 1}}
          behavior={Platform.OS == 'ios' ? 'padding' : 'height'}>
          <ScrollView
            style={{flex: 1}}
            //contentContainerStyle={{flexGrow: 1, flex: 1}}
            //bounces={false}
            showsVerticalScrollIndicator={false}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <View style={{alignItems: 'flex-end'}}>
                <Text
                  style={{
                    textAlign: 'center',
                    color: 'white',
                    fontSize: 32,
                    marginTop: 30,
                    alignSelf: 'center',
                    fontFamily: 'NunitoSans-ExtraBold',
                  }}>
                  gems
                </Text>
                {/* <Text
                  style={{
                    textAlign: 'center',
                    color: 'white',
                    fontSize: 14,
                    bottom: 28,
                    right: 20,
                    fontFamily: 'NunitoSans-Regular',
                  }}
                  onPress={() => this.props.navigation.navigate('DashBoard2')}>
                  Skip
                </Text>*/}
              </View> 
              <Formik
                initialValues={initialValues}
                validationSchema={validationRegister}
                onSubmit={(values, actions) => {
                  this.joinNowButton.showLoading(true);
                  // this.props.navigation.navigate('Welcome', {
                  //   first_name: values.first_name,
                  // });

                  fetch(this.props.appdata.RELEASE_API_URL + 'api/register', {
                    method: 'POST',
                    headers: {
                      'Content-Type': 'application/json',
                    },

                    body: JSON.stringify({
                      email: values.email,
                      password: values.password,
                      first_name: values.first_name,
                      last_name: values.last_name,
                    }),
                  })
                    .then((response) => response.json())
                    .then((responseJson) => {
                      this.joinNowButton.showLoading(false);

                      if (responseJson.success == true) {
                        this.props.navigation.navigate('Welcome', {
                          first_name: values.first_name,
                        });
                      } else {
                        actions.setErrors(responseJson);
                      }
                    })
                    .catch((error) => {
                      console.log(error);
                      alert('Please Try Again!');
                      this.joinNowButton.showLoading(false);
                    });
                }}
                validationSchema={validationRegister}>
                {(formikProps) => (
                  <View
                    style={{
                      backgroundColor: 'white',
                      flex: 1,
                      flexDirection: 'column',
                      //width: '100%',
                      marginTop: 0,
                      borderTopLeftRadius: 30,
                      borderTopRightRadius: 30,
                    }}>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222529',
                        fontSize: 30,
                        marginTop: 40,
                        alignSelf: 'center',
                        fontFamily: 'NunitoSans-Bold',
                      }}>
                      Create your account
                    </Text>
                    <TextInput
                      style={RegisterStyles.inputStyleBox}
                      placeholderTextColor="#0F2146"
                      placeholder="First name"
                      //underlineColorAndroid="rgba(0,0,0,0)"
                      keyboardType="default"
                      returnKeyType="next"
                      fontFamily="NunitoSans-Regular"
                      //onChangeText={(text) => this.setState({userName: text})}
                      value={formikProps.values.first_name}
                      onChangeText={formikProps.handleChange('first_name')}
                      onBlur={formikProps.handleBlur('first_name')}
                      blurOnSubmit={false}
                      autoCorrect={false}
                      disableFullscreenUI={true}
                      onSubmitEditing={() => {
                        this.secondTextInput.focus();
                      }}

                      //onBlur={() => this._CheckTConditionChacked()}
                      //onFocus={() => this._onFocusChacked()}
                    />
                    {formikProps.touched.first_name &&
                    formikProps.errors.first_name ? (
                      <Text style={RegisterStyles.errMsg}>
                        {formikProps.touched.first_name &&
                          formikProps.errors.first_name}
                      </Text>
                    ) : null}

                    <TextInput
                      ref={(input) => {
                        this.secondTextInput = input;
                      }}
                      style={RegisterStyles.inputStyleBox}
                      placeholderTextColor="#0F2146"
                      placeholder="Last name"
                      //underlineColorAndroid="rgba(0,0,0,0)"
                      keyboardType="default"
                      returnKeyType="next"
                      //onChangeText={(text) => this.setState({userName: text})}
                      //value={this.state.userName}
                      autoCorrect={false}
                      disableFullscreenUI={true}
                      onSubmitEditing={() => {
                        this.thirdTextInput.focus();
                      }}
                      onChangeText={formikProps.handleChange('last_name')}
                      onBlur={formikProps.handleBlur('last_name')}
                      blurOnSubmit={false}
                      //onBlur={() => this._CheckTConditionChacked()}
                      //onFocus={() => this._onFocusChacked()}
                    />
                    {formikProps.touched.last_name &&
                    formikProps.errors.last_name ? (
                      <Text style={RegisterStyles.errMsg}>
                        {formikProps.touched.last_name &&
                          formikProps.errors.last_name}
                      </Text>
                    ) : null}
                    <TextInput
                      ref={(input) => {
                        this.thirdTextInput = input;
                      }}
                      style={RegisterStyles.inputStyleBox}
                      placeholderTextColor="#0F2146"
                      placeholder="Email address"
                      //underlineColorAndroid="rgba(0,0,0,0)"
                      keyboardType="email-address"
                      returnKeyType="next"
                      //onChangeText={(text) => this.setState({userName: text})}
                      //value={this.state.userName}
                      autoCorrect={false}
                      disableFullscreenUI={true}
                      autoCapitalize="none"
                      onSubmitEditing={() => {
                        this.fourthTextInput.focus();
                      }}
                      blurOnSubmit={false}
                      //onBlur={() => this._CheckTConditionChacked()}
                      //onFocus={() => this._onFocusChacked()}
                      onChangeText={formikProps.handleChange('email')}
                      onBlur={formikProps.handleBlur('email')}
                    />
                    {formikProps.touched.email && formikProps.errors.email ? (
                      <Text style={RegisterStyles.errMsg}>
                        {formikProps.touched.email && formikProps.errors.email}
                      </Text>
                    ) : null}

                    <TextInput
                      ref={(input) => {
                        this.fourthTextInput = input;
                      }}
                      style={RegisterStyles.inputStyleBox}
                      placeholderTextColor="#0F2146"
                      placeholder="Password"
                      secureTextEntry={true}
                      //underlineColorAndroid="rgba(0,0,0,0)"
                      keyboardType="default"
                      returnKeyType="done"
                      //onChangeText={(text) => this.setState({userName: text})}
                      //value={this.state.userName}
                      autoCorrect={false}
                      disableFullscreenUI={true}
                      autoCapitalize="none"
                      //onSubmitEditing={() => { this.secondTextInput.focus(); }}
                      blurOnSubmit={true}
                      //onBlur={() => this._CheckTConditionChacked()}
                      //onFocus={() => this._onFocusChacked()}
                      onChangeText={formikProps.handleChange('password')}
                      onBlur={formikProps.handleBlur('password')}
                    />
                    {formikProps.touched.password &&
                    formikProps.errors.password ? (
                      <Text style={RegisterStyles.errMsg}>
                        {formikProps.touched.password &&
                          formikProps.errors.password}
                      </Text>
                    ) : null}
                    <View
                      style={{
                        justifyContent: 'center',
                        margin: 25,
                        marginTop: 40,
                      }}>
                      <Text
                        style={{
                          textAlign: 'center',
                          fontSize: 14,
                          color: '#0F2146',
                          fontFamily: 'NunitoSans-Regular',
                        }}>
                        By clicking 'join now', you agree to gems
                        <Text style={{color: '#3D73DD'}}>
                          {' '}
                          User Agreement, Privacy Policy {''}
                        </Text>
                        and {''}
                        <Text style={{color: '#3D73DD'}}>Cookie Policy, </Text>
                      </Text>
                      <View
                        style={{
                          flex: 1,
                          backgroundColor: 'rgb(255,255,255)',
                          justifyContent: 'center',
                          margin: 20,
                          marginTop: 30,
                        }}>
                        <AnimateLoadingButton
                          ref={(c) => (this.joinNowButton = c)}
                          height={60}
                          width={Dimensions.get('window').width - 40}
                          title="Join now"
                          titleFontSize={18}
                          titleColor="rgb(255,255,255)"
                          backgroundColor="#3D73DD"
                          borderRadius={10}
                          titleFontFamily={'NunitoSans-Regular'}
                          onPress={formikProps.handleSubmit}
                          //onPress={this.loginService()}
                          //onPress={this._onPressHandler.bind(this)}
                          //style={{fontFamily:'Arial'}}
                        />
                      </View>

                      <View
                        style={{
                          flex: 1,
                          flexDirection: 'row',
                          margin: 0,
                          marginTop: 40,
                          //backgroundColor: 'red'
                        }}>
                        <View
                          style={{
                            height: 1,
                            width: Dimensions.get('window').width / 2 - 40,
                            backgroundColor: '#E1E9F0',
                            margin: 0,
                          }}></View>
                        <Text
                          style={{
                            alignSelf: 'center',
                            color: '#0F2146',
                            fontSize: 14,
                            margin: 10,
                            marginTop: -10,
                            fontFamily: 'NunitoSans-Regular',
                          }}>
                          or
                        </Text>
                        <View
                          style={{
                            height: 1,
                            width: Dimensions.get('window').width / 2 - 40,
                            backgroundColor: '#E1E9F0',
                            margin: 0,
                          }}></View>
                      </View>

                      <View
                        style={{
                          flex: 1,
                          backgroundColor: 'rgb(255,255,255)',
                          //justifyContent: 'center',
                          margin: 20,
                          marginTop: 40,
                          marginBottom: 0,
                          backgroundColor: '#3D73DD',
                        }}>
                        <AnimateLoadingButton
                          ref={(c) => (this.loadingButton = c)}
                          height={60}
                          width={Dimensions.get('window').width - 40}
                          title="Sign in with Apple"
                          titleFontSize={18}
                          titleColor="rgb(255,255,255)"
                          borderRadius={10}
                          backgroundColor="#222529"
                          titleFontFamily={'NunitoSans-Regular'}
                          //onPress={this.loginService()}
                          onPress={this._onPressHandler.bind(this)}
                          //style={{fontFamily:'Arial'}}
                        ></AnimateLoadingButton>
                        <Image
                          source={require('../Images/Apple.png')}
                          style={{
                            //backgroundColor: 'red',
                            position: 'absolute',

                            height: 22,
                            width: 17,
                            margin: 20,
                            marginTop: 18,
                          }}></Image>
                      </View>

                      <View
                        style={{
                          flex: 1,
                          backgroundColor: 'rgb(255,255,255)',
                          //justifyContent: 'center',
                          margin: 20,
                          marginTop: 20,
                          marginBottom: 0,
                          backgroundColor: '#3D73DD',
                        }}>
                        <AnimateLoadingButton
                          ref={(c) => (this.googleButton = c)}
                          height={60}
                          width={Dimensions.get('window').width - 40}
                          title="Sign in with Google"
                          titleFontSize={18}
                          titleColor="rgb(255,255,255)"
                          borderRadius={10}
                          backgroundColor="#FF5C5C"
                          titleFontFamily={'NunitoSans-Regular'}
                          onPress={this._signIn}
                          //onPress={this._onPressHandler.bind(this)}
                          //style={{fontFamily:'Arial'}}
                        ></AnimateLoadingButton>
                        <Image
                          source={require('../Images/Google.png')}
                          style={{
                            // backgroundColor: 'red',
                            position: 'absolute',

                            height: 22,
                            width: 17,
                            margin: 20,
                            marginTop: 18,
                          }}></Image>
                      </View>
                      <View
                        style={{
                          flex: 1,
                          backgroundColor: 'rgb(255,255,255)',
                          //justifyContent: 'center',
                          margin: 20,
                          marginTop: 20,
                          marginBottom: 0,
                          backgroundColor: '#3D73DD',
                        }}>
                        <AnimateLoadingButton
                          ref={(c) => (this.facebookButton = c)}
                          height={60}
                          width={Dimensions.get('window').width - 40}
                          title="Sign in with Facebook"
                          titleFontSize={18}
                          titleColor="rgb(255,255,255)"
                          borderRadius={10}
                          backgroundColor="#3F5892"
                          onPress={() => this.loginFacebook()}
                          titleFontFamily={'NunitoSans-Regular'}
                          //onPress={this._onPressHandler.bind(this)}
                          //style={{fontFamily:'Arial'}}
                        ></AnimateLoadingButton>
                        <Image
                          source={require('../Images/Facebook.png')}
                          style={{
                            //backgroundColor: 'red',
                            position: 'absolute',

                            height: 22,
                            width: 11,
                            margin: 20,
                          }}></Image>
                      </View>

                      <View
                        style={{
                          height: 1,
                          margin: 0,
                          marginTop: 40,
                          backgroundColor: '#E1E9F0',
                        }}></View>

                      <View style={{flex: 1, margin: 20}}>
                        <Text
                          style={{
                            textAlign: 'center',
                            fontSize: 18,
                            color: '#0F2146',
                            fontFamily: 'NunitoSans-Regular',
                          }}>
                          Already on Gems?{' '}
                          <Text
                            style={{fontSize: 18, color: '#3D73DD'}}
                            onPress={() => this.redirectLogin()}>
                            Sign in
                          </Text>
                        </Text>
                      </View>

                      {/* <Text
                    style={{
                      marginLeft: 20,
                      fontFamily: 'Arial',
                      fontSize: 15,
                      color: '#0F2146',
                    }}>
                    I agree with&nbsp;
                    <Text
                      style={{
                        fontFamily: 'Arial',
                        fontSize: 15,
                        textDecorationLine: 'underline',
                      }}
                      onPress={this.TermsConditionClicked}>
                      Terms &amp; Conditions
                    </Text>{' '}
                    and{' '}
                    <Text
                      style={{
                        fontFamily: 'Arial',
                        fontSize: 15,
                        textDecorationLine: 'underline',
                      }}
                      onPress={this.PrivacyPolicyClicked}>
                      Privacy Policy
                    </Text>
                  </Text>*/}
                    </View>
                  </View>
                )}
              </Formik>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

const RegisterStyles = StyleSheet.create({
  inputStyleBox: {
    height: 60,
    margin: 20,
    backgroundColor: '#F2F5F8',
    borderRadius: 5,
    fontSize: 17,
    fontFamily: 'Arial',
    padding: 20,
    color: '#0F2146',
    marginBottom: 0,
    fontFamily: 'NunitoSans-Regular',
  },
  errMsg: {
    color: 'red',

    fontSize: 16,
    letterSpacing: 1,
    marginTop: 5,
    marginLeft: 20,
  },
});

const mapStateToProps = (state) => {
  const {appdata} = state;
  return {appdata};
};

export default connect(mapStateToProps)(CreateAccount);
