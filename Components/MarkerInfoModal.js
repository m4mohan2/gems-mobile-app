import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Modal,
  TextInput,
  TouchableOpacity,
  Image,
  Alert,
  Linking,
  Dimensions,
  ImageBackground,
} from 'react-native';
import AnimateLoadingButton from 'react-native-animate-loading-button';

export default class MarkerInfoModal extends Component {
  constructor(props) {
    super(props);
    console.log('kk', this.props);
  }
  componentDidMount() {}

  hideModal = (val) => {
    this.props.updateAleartParent(val);
  };

  closeModal = () => {
    console.log('close');
  };

  render() {
    console.log(JSON.stringify(this.props.newvalue));
    return (
      <Modal
        transparent={true}
        onRequestClose={() => this.closeModal}
        animationType="slide">
        <View style={styles.View_1dropDown}>
          <TouchableOpacity
            style={{flex: 1}}
            onPress={() => this.hideModal(1)}></TouchableOpacity>
          <View
            style={{
              margin: 20,
              width: '90%',
              backgroundColor: 'white',
              justifyContent: 'center',
              alignSelf: 'center',
              padding: 10,
              marginTop: 0,
              marginBottom: 100,
              paddingBottom: 10,
              borderRadius: 10,
            }}>
            <ImageBackground
              style={{
                width: '100%',
                //backgroundColor: '#4C453F',
                //height: 50,

                // padding: 20,
                // paddingTop: 10,
                // paddingBottom: 10,
                //borderRadius: 10,
              }}
              imageStyle={{borderRadius: 10}}
              source={require('../Images/BaconStrips.png')}>
              <View
                style={{
                  width: '100%',
                  backgroundColor: '#4C453F',
                  opacity: 0.9,
                  borderRadius: 10,
                  paddingLeft: 20,
                  paddingRight: 20,
                  paddingTop: 10,
                  paddingBottom: 10,
                }}>
                <Text
                  style={{
                    color: 'white',
                    fontSize: 30,
                    fontFamily: 'NunitoSans-Bold',
                  }}>
                  {this.props.newvalue.name}
                </Text>
                <Text
                  style={{
                    color: 'white',
                    fontSize: 17,
                    fontFamily: 'NunitoSans-Regular',
                  }}>
                  {this.props.newvalue.types[0] +
                    ' ' +
                    this.props.newvalue.types[1]}
                </Text>
              </View>
            </ImageBackground>

            <View
              style={{
                backgroundColor: '#F2F5F8',
                justifyContent: 'center',
                margin: 20,
                marginTop: 10,
                padding: 0,

                marginBottom: 0,
              }}>
              <AnimateLoadingButton
                ref={(c) => (this.joinNowButton = c)}
                height={60}
                width={Dimensions.get('window').width - 60}
                title="Add Gem"
                titleFontSize={18}
                titleColor="rgb(255,255,255)"
                backgroundColor="#3D73DD"
                borderRadius={10}
                titleFontFamily={'NunitoSans-Regular'}
                onPress={() => this.hideModal(2)}
                //onPress={this.loginService()}
                //onPress={this._onPressHandler.bind(this)}
                //style={{fontFamily:'Arial'}}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  View_1dropDown: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    //width: 200,
    //height: 200,
    //backgroundColor: 'rgba(52, 52, 52, 0.6)',
    justifyContent: 'flex-end',
  },
  Lbl_Style: {
    //color: '#6d6b6b',
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
  },
});
