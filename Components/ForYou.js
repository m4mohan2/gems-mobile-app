import React, {Component} from 'react';

import {
  SafeAreaView,
  View,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  Image,
  ToastAndroid,
  BackHandler,
} from 'react-native';

class ForYou extends Component {
  constructor(props) {
    super(props);
    this.state = {count: 0};
  }
  goProfile = () => {
    this.props.navigation.navigate('Profile', {
      refresh: false,
    });
  };
  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButton.bind(this),
    );
  }
  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButton.bind(this),
    );
  }
  handleBackButton() {
    if (this.state.count == 0) {
      this.setState({count: this.state.count + 1});
      ToastAndroid.show('press back again to exist the app', 300);

      setTimeout(() => {
        this.setState({count: 0});
      }, 10000);
      //   let timer = setInterval(this.tick, 10);
      // this.setState({timer});
      return true;
    } else {
      BackHandler.exitApp();
    }
  }
  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#191C1F'}}>
        <View
          style={{
            flex: 1,
            // alignItems: 'center',
            // justifyContent: 'center',
            backgroundColor: '#F2F5F8',
            borderBottomRightRadius: 30,
            borderBottomLeftRadius: 30,
          }}>
          <View
            style={{
              height: 55,
              margin: 10,
              marginTop: 20,
              borderRadius: 10,
              backgroundColor: '#FFFFFF',
              flexDirection: 'row-reverse',
            }}>
            <TouchableOpacity
              style={{
                height: 55,
                marginTop: 0,
                marginEnd: 0,
                width: 60,
                //backgroundColor: 'red',
              }}
              onPress={this.goProfile}>
              <Image
                style={{
                  width: 40,
                  height: 37,
                  marginTop: 10,
                }}
                source={require('../Images/Profile.png')}
                resizeMode="contain"
                resizeMethod="scale"></Image>
            </TouchableOpacity>
            {/* <View
              style={{
                width: 1,
                height: 35,
                backgroundColor: '#CCD2D9',
                margin: 10,
              }}></View> */}
            <TextInput
              style={{
                left: 0,
                marginTop: 0,
                marginRight: 10,
                marginBottom: 0,
                marginLeft: 0,
                flex: 1,
                paddingRight: 10,
                paddingLeft: 20,
                fontSize: 17,
                fontFamily: 'NunitoSans-Regular',
              }}
              placeholder="Search..."
              placeholderTextColor="#0F2146"></TextInput>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const RegisterStyles = StyleSheet.create({
  inputStyleBox: {
    height: 60,
    margin: 20,
    backgroundColor: '#F2F5F8',
    borderRadius: 5,
    fontSize: 17,
    fontFamily: 'NunitoSans-Regular',
    padding: 20,
    color: '#0F2146',
    marginBottom: 0,
  },
});

export default ForYou;
