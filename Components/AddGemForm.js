import React, {Component} from 'react';

import {
  SafeAreaView,
  View,
  KeyboardAvoidingView,
  ScrollView,
  Image,
  TouchableOpacity,
  Dimensions,
  ImageBackground,
  StyleSheet,
  TextInput,
  Text,
  FlatList,
  ToastAndroid,
  BackHandler,
} from 'react-native';
import AnimateLoadingButton from 'react-native-animate-loading-button';
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from 'react-native-image-crop-picker';
import Geocoder from 'react-native-geocoding';
Geocoder.init('AIzaSyDC7qyMTTAr0YzGKCKMU4vje4uKfD8lKso', {language: 'en'});

import {connect} from 'react-redux';
import CustomModal from './CustomModal';
import CustomAlert from './CustomAlert';

class AddGemForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showCustomModal: false,
      CustomAlertShow: false,
      coverPhotoUrl: null,
      selectedPhoto: null,
      categoryList: [],
      selectcategory: null,
      gemName: null,
      gemAddress:
        this.props.route.params?.draggable == true
          ? this.props.route.params?.address
          : null,
      gemPhone: null,
      gemsPrivacy: 2,
      isGemsManual:
        this.props.route.params?.gemInfo == undefined ? true : false,
      gemInfo:
        this.props.route.params?.gemInfo == undefined
          ? null
          : this.props.route.params?.gemInfo,
      count: 0,
      gemAdded: false,
    };
    console.log('distance', this.props.route.params);
  }
  getPhoto = () => {
    console.log('call');
    const url =
      'https://maps.googleapis.com/maps/api/place/photo?maxwidth=' +
      this.state.gemInfo.photos[0].width +
      '&photoreference=' +
      this.state.gemInfo.photos[0].photo_reference +
      '&key=AIzaSyDC7qyMTTAr0YzGKCKMU4vje4uKfD8lKso';

    console.log(url);
    fetch(url)
      .then((response) => response)
      .then((JsonResponse) => {
        console.log(JsonResponse);
        if (JsonResponse.status == 200) {
          this.setState({coverPhotoUrl: JsonResponse.url});
        }
      })
      .catch((error) => {
        alert(error);
      });
  };

  getCategory = () => {
    AsyncStorage.getItem('userInfo').then(
      (value) => {
        const jsonValue = JSON.parse(value);
        console.log('======', jsonValue.token);

        fetch(this.props.appdata.RELEASE_API_URL + 'api/listCategory', {
          method: 'GET',
          headers: {
            token: 'Bearer ' + jsonValue.token,
          },
        })
          .then((response) => response.json())
          .then((responseJson) => {
            console.log('++++', responseJson);
            this.setState({categoryList: responseJson}, () =>
              console.log('category State', this.state.categoryList),
            );
          })
          .catch((error) => {
            console.log(error);
          });
      },
      //AsyncStorage returns a promise so adding a callback to get the value

      //Setting the value in Text
    );
  };
  update_Custom_modal = () => {
    this.setState({showCustomModal: false});

    if (this.state.gemAdded == true) {
      this.setState({gemAdded: false});
      this.redirectMap();
    }
  };
  createGems = () => {
    if (this.state.isGemsManual == true) {
      if (this.props.route.params?.draggable == true) {
        if (this.state.gemName == null || this.state.gemName == '') {
          this.setState({
            showCustomModal: true,
            AlertValue: {
              AlertHedingTitle: 'gems',
              AlertTitle: 'Please enter a name',
              success: false,
              AlertOkTitle: 'OK',
              SingleButton: true,
            },
          });
        } else if (this.state.selectcategory == null) {
          this.setState({
            showCustomModal: true,
            AlertValue: {
              AlertHedingTitle: 'gems',
              AlertTitle: 'Please select a category',
              success: false,
              AlertOkTitle: 'OK',
              SingleButton: true,
            },
          });
        } else if (this.state.selectcategory == null) {
          this.setState({
            showCustomModal: true,
            AlertValue: {
              AlertHedingTitle: 'gems',
              AlertTitle: 'Please select a category',
              success: false,
              AlertOkTitle: 'OK',
              SingleButton: true,
            },
          });
        } else {
          this.joinNowButton.showLoading(true);

          AsyncStorage.getItem('userInfo').then((value) => {
            const jsonValue = JSON.parse(value);

            fetch(this.props.appdata.RELEASE_API_URL + 'api/createGem', {
              method: 'POST',
              headers: {
                token: 'Bearer ' + jsonValue.token,
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                category_id: this.state.selectcategory.id,
                name: this.state.gemName,
                lat: this.props.route.params?.location.lat,
                lng: this.props.route.params?.location.lng,
                address: this.props.route.params?.address,
                place_id: this.props.route.params?.place_id,
              }),
            })
              .then((response) => response.json())
              .then((responseJson) => {
                console.log('++++', responseJson);
                this.joinNowButton.showLoading(false);

                if (responseJson.success == false) {
                  this.setState({
                    showCustomModal: true,
                    gemAdded: false,
                    AlertValue: {
                      AlertHedingTitle: 'gems',
                      AlertTitle: responseJson.message,
                      success: false,
                      AlertOkTitle: 'OK',
                      SingleButton: true,
                    },
                  });
                } else {
                  this.setState({
                    showCustomModal: true,
                    gemAdded: true,
                    AlertValue: {
                      AlertHedingTitle: 'gems',
                      AlertTitle: responseJson.message,
                      success: true,
                      AlertOkTitle: 'OK',
                      SingleButton: true,
                    },
                  });
                }
              })
              .catch((error) => {
                console.log(error);

                this.joinNowButton.showLoading(false);
                this.setState({
                  showCustomModal: true,
                  AlertValue: {
                    AlertHedingTitle: 'gems',
                    AlertTitle:
                      'Due to some technical issue, unable to add gems',
                    success: false,
                    AlertOkTitle: 'OK',
                    SingleButton: true,
                  },
                });
              });
          });
        }
      } else {
        if (this.state.gemName == null || this.state.gemName == '') {
          this.setState({
            showCustomModal: true,
            AlertValue: {
              AlertHedingTitle: 'gems',
              AlertTitle: 'Please enter a name',
              success: false,
              AlertOkTitle: 'OK',
              SingleButton: true,
            },
          });
        } else if (
          this.state.gemAddress == null ||
          this.state.gemAddress == ''
        ) {
          this.setState({
            showCustomModal: true,
            AlertValue: {
              AlertHedingTitle: 'gems',
              AlertTitle: 'Please enter address',
              success: false,
              AlertOkTitle: 'OK',
              SingleButton: true,
            },
          });
        } else if (this.state.selectcategory == null) {
          this.setState({
            showCustomModal: true,
            AlertValue: {
              AlertHedingTitle: 'gems',
              AlertTitle: 'Please select a category',
              success: false,
              AlertOkTitle: 'OK',
              SingleButton: true,
            },
          });
        } else {
          this.joinNowButton.showLoading(true);

          Geocoder.from(this.state.gemAddress)
            .then((json) => {
              var location = json.results[0].geometry.location;
              console.log(location);
              console.log(json);
              AsyncStorage.getItem('userInfo').then((value) => {
                const jsonValue = JSON.parse(value);

                fetch(this.props.appdata.RELEASE_API_URL + 'api/createGem', {
                  method: 'POST',
                  headers: {
                    token: 'Bearer ' + jsonValue.token,
                    'Content-Type': 'application/json',
                  },
                  body: JSON.stringify({
                    category_id: this.state.selectcategory.id,
                    name: this.state.gemName,
                    lat: location.lat,
                    lng: location.lng,
                    address: this.state.gemAddress,
                    place_id: json.results[0].place_id,
                  }),
                })
                  .then((response) => response.json())
                  .then((responseJson) => {
                    console.log('++++', responseJson);
                    this.joinNowButton.showLoading(false);
                    if (responseJson.success == false) {
                      this.setState({
                        showCustomModal: true,
                        gemAdded: false,
                        AlertValue: {
                          AlertHedingTitle: 'gems',
                          AlertTitle: responseJson.message,
                          success: false,
                          AlertOkTitle: 'OK',
                          SingleButton: true,
                        },
                      });
                    } else {
                      this.setState({
                        showCustomModal: true,
                        gemAdded: true,
                        AlertValue: {
                          AlertHedingTitle: 'gems',
                          AlertTitle: responseJson.message,
                          success: true,
                          AlertOkTitle: 'OK',
                          SingleButton: true,
                        },
                      });
                    }
                  })
                  .catch((error) => {
                    console.log(error);

                    this.joinNowButton.showLoading(false);
                    this.setState({
                      showCustomModal: true,
                      AlertValue: {
                        AlertHedingTitle: 'gems',
                        AlertTitle:
                          'Due to some technical issue, unable to add gems',
                        success: false,
                        AlertOkTitle: 'OK',
                        SingleButton: true,
                      },
                    });
                  });
              });
            })
            .catch((error) => console.warn(error));
        }
      }
    } else {
      console.log('here');
      if (this.state.selectcategory == null) {
        this.setState({
          showCustomModal: true,
          AlertValue: {
            AlertHedingTitle: 'gems',
            AlertTitle: 'Please select a category',
            success: false,
            AlertOkTitle: 'OK',
            SingleButton: true,
          },
        });
      } else {
        this.joinNowButton.showLoading(true);
        AsyncStorage.getItem('userInfo').then((value) => {
          const jsonValue = JSON.parse(value);

          fetch(this.props.appdata.RELEASE_API_URL + 'api/createGem', {
            method: 'POST',
            headers: {
              token: 'Bearer ' + jsonValue.token,
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              category_id: this.state.selectcategory.id,
              name: this.state.gemInfo.name,
              lat: this.state.gemInfo.geometry.location.lat,
              lng: this.state.gemInfo.geometry.location.lng,
              address:
                this.state.gemInfo.vicinity == undefined ||
                this.state.gemInfo.vicinity == null
                  ? this.state.gemInfo.formatted_address
                  : this.state.gemInfo.vicinity,
              place_id: this.state.gemInfo.place_id,
            }),
          })
            .then((response) => response.json())
            .then((responseJson) => {
              console.log('++++', responseJson);
              this.joinNowButton.showLoading(false);
              if (responseJson.success == false) {
                this.setState({
                  showCustomModal: true,
                  gemAdded: false,
                  AlertValue: {
                    AlertHedingTitle: 'gems',
                    AlertTitle: responseJson.message,
                    success: false,
                    AlertOkTitle: 'OK',
                    SingleButton: true,
                  },
                });
              } else {
                this.setState({
                  showCustomModal: true,
                  gemAdded: true,
                  AlertValue: {
                    AlertHedingTitle: 'gems',
                    AlertTitle: responseJson.message,
                    success: true,
                    AlertOkTitle: 'OK',
                    SingleButton: true,
                  },
                });
              }
            })
            .catch((error) => {
              console.log(error);

              this.joinNowButton.showLoading(false);
              this.setState({
                showCustomModal: true,
                AlertValue: {
                  AlertHedingTitle: 'gems',
                  AlertTitle: error,
                  success: false,
                  AlertOkTitle: 'OK',
                  SingleButton: true,
                },
              });
            });
        });
      }
    }
  };
  update_Aleart = (value) => {
    this.setState({CustomAlertShow: false});
    console.log(value);

    if (value == 1) {
      ImagePicker.openCamera({
        width: 300,
        height: 400,
        cropping: true,
      }).then((image) => {
        console.log(image);
        this.setState({selectedPhoto: image});
      });
    } else if (value == 2) {
      ImagePicker.openPicker({
        width: 300,
        height: 400,
        cropping: true,
        includeBase64: true,
      }).then((image) => {
        console.log(image);
        this.setState({selectedPhoto: image});
      });
    }
  };
  changeImage = () => {
    ImagePicker.clean()
      .then(() => {
        console.log('removed all tmp images from tmp directory');
      })
      .catch((e) => {
        alert(e);
      });

    this.setState({CustomAlertShow: true});
  };
  componentDidMount() {
    console.log('did mount');
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButton.bind(this),
    );

    if (this.state.gemInfo !== null) {
      if (this.state.gemInfo.photos) {
        this.getPhoto();
      }
    }
    this.getCategory();
  }
  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButton.bind(this),
    );
  }

  handleBackButton() {
    if (this.state.count == 0) {
      this.setState({count: this.state.count + 1});
      ToastAndroid.show('press back again to exist the app', 300);

      setTimeout(() => {
        this.setState({count: 0});
      }, 10000);
      //   let timer = setInterval(this.tick, 10);
      // this.setState({timer});
      return true;
    } else {
      BackHandler.exitApp();
    }
  }
  select_Category = (item) => {
    console.log(item);
    this.setState({selectcategory: item});
  };

  choosePrivacy = (val) => {
    this.setState({gemsPrivacy: val});
  };
  redirectMap = () => {
    if (this.props.route.params?.draggable == true) {
      this.props.navigation.navigate('AddGems', {
        draggable: true,
      });
    } else {
      this.props.route.params?.refresh();
      this.props.navigation.navigate('AddGems');
    }
  };
  render() {
    console.log('render');
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: 'black'}}>
        <KeyboardAvoidingView
          style={{flex: 1, backgroundColor: '#F2F5F8'}}
          behavior={Platform.OS == 'ios' ? 'padding' : 'height'}>
          <ScrollView style={{flex: 1}} showsVerticalScrollIndicator={false}>
            <View
              style={{
                flex: 1,
                flexDirection: 'column',
              }}>
              <ImageBackground
                imageStyle={{
                  borderBottomLeftRadius: 20,
                  borderBottomRightRadius: 20,
                }}
                style={{
                  height: 350,
                  width: Dimensions.get('window').width,
                  marginTop: 0,

                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                source={
                  this.state.coverPhotoUrl == null
                    ? this.state.selectedPhoto == null
                      ? null
                      : {uri: this.state.selectedPhoto.path}
                    : {uri: this.state.coverPhotoUrl}
                }>
                <Image
                  style={{height: 108, width: 100}}
                  source={
                    this.state.coverPhotoUrl == null
                      ? require('../Images/AddPicture.png')
                      : null
                  }></Image>
                <TouchableOpacity
                  style={{
                    height: 108,
                    width: 100,
                    //backgroundColor: 'red',
                    position: 'absolute',
                  }}
                  onPress={() => this.changeImage()}></TouchableOpacity>
              </ImageBackground>
              <TouchableOpacity
                style={{
                  height: 40,
                  width: 40,
                  position: 'absolute',
                  marginTop: 20,
                  right: 10,
                }}
                onPress={this.redirectMap}>
                <View
                  style={{
                    position: 'absolute',
                    marginTop: 0,
                    width: 2,
                    height: 16,
                    backgroundColor: '#222529',
                    right: 10,
                    transform: [{rotate: '45deg'}],
                  }}></View>
                <View
                  style={{
                    position: 'absolute',
                    marginTop: 0,
                    width: 2,
                    height: 16,
                    backgroundColor: '#222529',
                    right: 10,
                    transform: [{rotate: '-45deg'}],
                  }}></View>
              </TouchableOpacity>
              {this.state.gemInfo == null ? (
                <View>
                  <TextInput
                    style={RegisterStyles.inputStyleBox}
                    placeholderTextColor="#A6AFBD"
                    placeholder="Name of restaurant"
                    //underlineColorAndroid="rgba(0,0,0,0)"
                    keyboardType="default"
                    returnKeyType="next"
                    onChangeText={(text) => {
                      this.setState({gemName: text});
                      if (text.length > 0) {
                        this.setState({nameError: false});
                      }
                    }}
                    // value={
                    //   this.state.gemInfo == null ? '' : this.state.gemInfo.name
                    // }
                    //onChangeText={formikProps.handleChange('first_name')}
                    // onBlur={formikProps.handleBlur('first_name')}
                    blurOnSubmit={false}
                    autoCorrect={false}
                    disableFullscreenUI={true}
                    onSubmitEditing={() => {
                      this.secondTextInput.focus();
                    }}

                    //onBlur={() => this._CheckTConditionChacked()}
                    //onFocus={() => this._onFocusChacked()}
                  />
                  {this.state.nameError == true ? (
                    <Text style={RegisterStyles.errMsg}>
                      Please enter a name.
                    </Text>
                  ) : null}
                  <TextInput
                    ref={(input) => {
                      this.secondTextInput = input;
                    }}
                    style={RegisterStyles.inputStyleBox}
                    placeholderTextColor="#A6AFBD"
                    placeholder="Address"
                    multiline={true}
                    numberOfLines={0}
                    //underlineColorAndroid="rgba(0,0,0,0)"
                    keyboardType="default"
                    returnKeyType="next"
                    onChangeText={(text) => {
                      this.setState({gemAddress: text});
                      if (text.length > 0) {
                        this.setState({addressError: false});
                      }
                    }}
                    value={this.state.gemAddress}
                    editable={
                      this.props.route.params?.draggable == true ? false : true
                    }
                    // value={
                    //   this.state.gemInfo == null
                    //     ? ''
                    //     : this.state.gemInfo.vicinity == null
                    //     ? ''
                    //     : this.state.gemInfo.vicinity
                    // }
                    //onChangeText={formikProps.handleChange('first_name')}
                    // onBlur={formikProps.handleBlur('first_name')}
                    blurOnSubmit={false}
                    autoCorrect={false}
                    disableFullscreenUI={true}

                    //onBlur={() => this._CheckTConditionChacked()}
                    //onFocus={() => this._onFocusChacked()}
                  />
                  {this.state.addressError == true ? (
                    <Text style={RegisterStyles.errMsg}>
                      Please enter address.
                    </Text>
                  ) : null}

                  <TextInput
                    ref={(input) => {
                      this.thirdTextInput = input;
                    }}
                    style={RegisterStyles.inputStyleBox}
                    placeholderTextColor="#A6AFBD"
                    placeholder="Phone number"
                    //underlineColorAndroid="rgba(0,0,0,0)"
                    keyboardType="phone-pad"
                    returnKeyType="next"
                    onChangeText={(text) => this.setState({gemPhone: text})}
                    // value={formikProps.values.first_name}
                    //onChangeText={formikProps.handleChange('first_name')}
                    // onBlur={formikProps.handleBlur('first_name')}
                    blurOnSubmit={false}
                    autoCorrect={false}
                    disableFullscreenUI={true}
                    onSubmitEditing={() => {
                      this.secondTextInput.focus();
                    }}

                    //onBlur={() => this._CheckTConditionChacked()}
                    //onFocus={() => this._onFocusChacked()}
                  />
                </View>
              ) : (
                <View style={{paddingRight: 20}}>
                  <Text
                    style={{
                      fontSize: 30,
                      fontFamily: 'NunitoSans-Bold',
                      color: '#0F2146',
                      textAlign: 'left',
                      marginLeft: 20,
                      marginTop: 20,
                    }}>
                    {this.state.gemInfo.name}
                  </Text>
                  <Text
                    style={{
                      fontSize: 20,
                      fontFamily: 'NunitoSans-Regular',
                      color: '#A6AFBD',
                      textAlign: 'left',
                      marginLeft: 20,
                      marginTop: 0,
                    }}>
                    {this.state.gemInfo.types[0] +
                      ',' +
                      ' ' +
                      this.state.gemInfo.types[1]}
                  </Text>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      paddingRight: 20,
                      //backgroundColor: 'red',
                    }}>
                    <Image
                      style={{
                        height: 20,
                        width: 20,
                        //backgroundColor: 'red',
                        marginLeft: 20,
                        marginTop: 10,
                      }}
                      source={require('../Images/walkingIcon.jpg')}></Image>
                    <Text
                      style={{
                        marginLeft: 10,
                        fontSize: 16,
                        fontFamily: 'NunitoSans-Regular',
                        color: '#3D73DD',
                        marginTop: 10,
                        marginRight: 10,
                      }}>
                      {this.props.route.params?.distance}
                    </Text>
                    <Text
                      style={{
                        marginLeft: 10,
                        fontSize: 16,
                        fontFamily: 'NunitoSans-Regular',
                        color: '#3D73DD',
                        marginTop: 10,
                        flexShrink: 1,
                      }}>
                      {this.state.gemInfo.vicinity == undefined ||
                      this.state.gemInfo.vicinity == null
                        ? this.state.gemInfo.formatted_address
                        : this.state.gemInfo.vicinity}
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      paddingRight: 20,
                    }}>
                    <Image
                      style={{
                        height: 20,
                        width: 20,
                        //backgroundColor: 'red',
                        marginLeft: 20,
                        marginTop: 30,
                      }}
                      source={require('../Images/walkingIcon.jpg')}></Image>

                    <Text
                      style={{
                        marginLeft: 5,
                        fontSize: 16,
                        fontFamily: 'NunitoSans-Regular',
                        color: '#3D73DD',
                        marginTop: 28,
                        paddingRight: 20,
                      }}>
                      +1 212 9565151
                    </Text>
                  </View>
                </View>
              )}

              <Text
                style={{
                  margin: 20,
                  fontFamily: 'NunitoSans-Bold',
                  fontSize: 17,
                  textAlign: 'left',
                  color: '#0F2146',
                  marginBottom: 10,
                }}>
                Select categories
              </Text>
              <View style={{margin: 20, marginTop: 10}}>
                <FlatList
                  horizontal={true}
                  showsVerticalScrollIndicator={false}
                  data={this.state.categoryList}
                  key={(item) => item.id}
                  renderItem={({item}) => (
                    <View
                      style={{
                        margin: 5,
                        marginTop: 0,
                        borderWidth: 1,
                        borderColor: '#0F2146',
                        borderRadius: 10,
                        backgroundColor: '#F2F5F8',
                        backgroundColor:
                          this.state.selectcategory !== null &&
                          this.state.selectcategory.id == item.id
                            ? '#68B3FF'
                            : '#F2F5F8',
                      }}>
                      <TouchableOpacity
                        style={{flex: 1}}
                        onPress={() => this.select_Category(item)}>
                        <Text
                          style={{
                            color: 'black',
                            padding: 7,
                            fontSize: 17,
                            fontFamily: 'NunitoSans-Regular',
                          }}>
                          {item.category_name}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  )}
                />
              </View>
              <Text
                style={{
                  margin: 20,
                  fontFamily: 'NunitoSans-Bold',
                  fontSize: 17,
                  textAlign: 'left',
                  color: '#0F2146',
                  marginBottom: 10,
                }}>
                Share with
              </Text>
              <View
                style={{
                  height: 60,
                  margin: 20,
                  marginTop: 10,
                  backgroundColor: '#FFFFFF',
                  borderRadius: 10,
                  //justifyContent: 'center',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Image
                  style={{
                    height: 30,
                    width: 30,
                    borderRadius: 30 / 2,
                    backgroundColor: 'blue',
                    marginLeft: 20,
                  }}></Image>
                <Text
                  style={{
                    color: '#3D73DD',
                    fontSize: 17,
                    fontFamily: 'NunitoSans-Regular',
                    marginLeft: 20,
                    marginTop: -5,
                  }}>
                  New group
                </Text>
              </View>
              <View
                style={{
                  margin: 20,
                  height: 1,
                  backgroundColor: '#E1E9F0',
                  marginTop: 10,
                }}></View>
              <View
                style={{
                  height: 60,
                  margin: 20,
                  marginTop: 10,
                  backgroundColor: '#FFFFFF',
                  borderRadius: 10,
                  //justifyContent: 'center',
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginBottom: 10,
                }}>
                <TouchableOpacity
                  style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}
                  onPress={() => this.choosePrivacy(1)}>
                  <View
                    style={{
                      height: 21,
                      width: 21,
                      backgroundColor: '#E1E9F0',
                      borderRadius: 4,
                      marginLeft: 20,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    {this.state.gemsPrivacy == 1 ? (
                      <View
                        style={{
                          height: 12,
                          width: 12,
                          backgroundColor: '#3D73DD',
                          borderRadius: 4,
                        }}></View>
                    ) : null}
                  </View>
                  <Text
                    style={{
                      color: '#0F2146',
                      fontSize: 17,
                      fontFamily: 'NunitoSans-Regular',
                      marginLeft: 20,
                      marginTop: -5,
                    }}>
                    Gem is private {'  '}
                    <Text
                      style={{
                        color: '#3D73DD',
                        fontSize: 14,
                        fontFamily: 'NunitoSans-Regular',
                        marginLeft: 20,
                        marginTop: -5,
                      }}
                      numberOfLines={0}>
                      (only visable for me)
                    </Text>
                  </Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  height: 60,
                  margin: 20,
                  marginTop: 0,
                  backgroundColor: '#FFFFFF',
                  borderRadius: 10,
                  //justifyContent: 'center',

                  marginBottom: 10,
                }}>
                <TouchableOpacity
                  style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}
                  onPress={() => this.choosePrivacy(2)}>
                  <View
                    style={{
                      height: 21,
                      width: 21,
                      backgroundColor: '#E1E9F0',
                      borderRadius: 4,
                      marginLeft: 20,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    {this.state.gemsPrivacy == 2 ? (
                      <View
                        style={{
                          height: 12,
                          width: 12,
                          backgroundColor: '#3D73DD',
                          borderRadius: 4,
                        }}></View>
                    ) : null}
                  </View>
                  <Text
                    style={{
                      color: '#0F2146',
                      fontSize: 17,
                      fontFamily: 'NunitoSans-Regular',
                      marginLeft: 20,
                      marginTop: -5,
                    }}>
                    Gem is public
                  </Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  height: 60,
                  margin: 20,
                  marginTop: 0,
                  backgroundColor: '#FFFFFF',
                  borderRadius: 10,
                  //justifyContent: 'center',
                }}>
                <TouchableOpacity
                  style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}
                  onPress={() => this.choosePrivacy(3)}>
                  <View
                    style={{
                      height: 21,
                      width: 21,
                      backgroundColor: '#E1E9F0',
                      borderRadius: 4,
                      marginLeft: 20,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    {this.state.gemsPrivacy == 3 ? (
                      <View
                        style={{
                          height: 12,
                          width: 12,
                          backgroundColor: '#3D73DD',
                          borderRadius: 4,
                        }}></View>
                    ) : null}
                  </View>
                  <Text
                    style={{
                      color: '#0F2146',
                      fontSize: 17,
                      fontFamily: 'NunitoSans-Regular',
                      marginLeft: 20,
                      marginTop: -5,
                    }}>
                    Friends
                  </Text>
                </TouchableOpacity>
              </View>
              <Text
                style={{
                  margin: 20,
                  fontFamily: 'NunitoSans-Bold',
                  fontSize: 17,
                  textAlign: 'left',
                  color: '#0F2146',
                  marginBottom: 10,
                }}>
                Note
              </Text>
              <TextInput
                style={RegisterStyles.textViewBox}
                placeholderTextColor="#818DA1"
                placeholder="Add a note to help what you liked about this location… You can leave this blank."
                //underlineColorAndroid="rgba(0,0,0,0)"
                keyboardType="default"
                returnKeyType="done"
                multiline={true}
                //onChangeText={(text) => this.setState({userName: text})}
                // value={formikProps.values.first_name}
                //onChangeText={formikProps.handleChange('first_name')}
                // onBlur={formikProps.handleBlur('first_name')}
                blurOnSubmit={false}
                autoCorrect={false}
                disableFullscreenUI={true}

                //onBlur={() => this._CheckTConditionChacked()}
                //onFocus={() => this._onFocusChacked()}
              />
              <View
                style={{
                  flex: 1,
                  backgroundColor: 'rgb(255,255,255)',
                  justifyContent: 'center',
                  margin: 20,
                  marginTop: 30,
                }}>
                <AnimateLoadingButton
                  ref={(c) => (this.joinNowButton = c)}
                  height={60}
                  width={Dimensions.get('window').width - 40}
                  title="Create Gem"
                  titleFontSize={18}
                  titleColor="rgb(255,255,255)"
                  backgroundColor="#3D73DD"
                  borderRadius={10}
                  titleFontFamily={'NunitoSans-Regular'}
                  onPress={this.createGems}
                  //onPress={this.loginService()}
                  //onPress={this._onPressHandler.bind(this)}
                  //style={{fontFamily:'Arial'}}
                />
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
        {this.state.showCustomModal ? (
          <CustomModal
            newvalue={this.state.AlertValue}
            updateAleartParent={this.update_Custom_modal.bind(this)}
          />
        ) : null}
        {this.state.CustomAlertShow ? (
          <CustomAlert updateAleartParent={this.update_Aleart.bind(this)} />
        ) : null}
      </SafeAreaView>
    );
  }
}
const RegisterStyles = StyleSheet.create({
  inputStyleBox: {
    height: 60,
    margin: 20,
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
    fontSize: 17,

    padding: 20,
    color: '#0F2146',
    marginBottom: 0,
    marginTop: 10,
    paddingBottom: 5,
    paddingTop: 5,

    fontFamily: 'NunitoSans-SemiBoldItalic',
  },
  textViewBox: {
    height: 160,
    margin: 20,
    backgroundColor: '#D2DAE3',
    borderRadius: 10,
    fontSize: 18,

    padding: 20,
    color: '#0F2146',
    marginBottom: 10,
    marginTop: 10,
    fontFamily: 'NunitoSans-Italic',
  },
  errMsg: {
    color: 'red',

    fontSize: 16,
    letterSpacing: 1,
    marginTop: 5,
    marginLeft: 20,
  },
});
const mapStateToProps = (state) => {
  const {appdata} = state;
  return {appdata};
};

export default connect(mapStateToProps)(AddGemForm);
