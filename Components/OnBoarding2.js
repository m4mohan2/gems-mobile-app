import React, {Component} from 'react';
import {
  SafeAreaView,
  Image,
  ScrollView,
  View,
  Text,
  UIManager,
  LayoutAnimation,
  Animated,
  Easing,
} from 'react-native';
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';

UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);

class Introduction extends Component {
  constructor(props) {
    super(props);
    this.spinValue = new Animated.Value(0);
    this.state = {
      pageIndex: 0,
      startingX: -50,
      startingY: 10,
      changedDone: false,
    };
  }
  spin() {
    this.spinValue.setValue(0);
    Animated.timing(this.spinValue, {
      toValue: 80,
      duration: 1000,
      easing: Easing.linear,
      useNativeDriver: true,
    }).start();

    this.middlePosition();
  }
  startPosition = () => {
    LayoutAnimation.linear();
    this.setState({
      startingX: -50,
      startingY: 10,
    });
  };

  middlePosition = () => {
    LayoutAnimation.linear(() =>
      this.setState({
        startingX: 60,
        startingY: 10,
      }),
    );
  };

  lastPosition = () => {
    LayoutAnimation.linear();
    this.setState({
      startingX: 40,
      startingY: -150,
    });
  };

  onSwipe(gestureName) {
    const {SWIPE_UP, SWIPE_DOWN, SWIPE_LEFT, SWIPE_RIGHT} = swipeDirections;

    switch (gestureName) {
      case SWIPE_UP:
        console.log('UP');
        break;
      case SWIPE_DOWN:
        console.log('Down');
        break;
      case SWIPE_LEFT:
        this.props.navigation.navigate('OnBoarding_3');

        break;
      case SWIPE_RIGHT:
        console.log('Right');
        this.props.navigation.navigate('OnBoarding_1');
        break;
    }
  }
  render() {
    const config = {
      velocityThreshold: 0.3,
      directionalOffsetThreshold: 80,
    };

    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '190deg'],
    });
    console.log('+++++', this.state.changedDone);
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#222529'}}>
        <GestureRecognizer
          onSwipe={(direction) => this.onSwipe(direction)}
          config={config}
          style={{
            flex: 1,
          }}>
          <ScrollView style={{}} contentContainerStyle={{flexGrow: 1}}>
            <View
              style={{
                backgroundColor: '#FFFFFF',
                flex: 1,
                flexDirection: 'column',
              }}>
              <Image
                style={{
                  left: 50,
                  top: 30,
                  width: '100%',
                  paddingBottom: 0,
                  height: '50%',
                }}
                source={require('../Images/Blob1.png')}
                resizeMode="contain"
                resizeMethod="scale"></Image>
              {/* {this.state.pageIndex == 0 ? (
                <Image
                  style={{
                    left: this.state.startingX,
                    top: this.state.startingY,
                    width: '97%',
                    paddingBottom: 0,

                    //height:366,
                  }}
                  source={require('../Images/Blob.png')}
                  resizeMode="contain"
                  resizeMethod="scale"></Image>
              ) : this.state.pageIndex == 1 ? (
                <Image
                  style={{
                    left: this.state.startingX,
                    top: this.state.startingY,
                    width: '97%',
                    //transform: [{ rotate: '196deg' }]
                  }}
                  source={require('../Images/Blob.png')}
                  resizeMode="contain"></Image>
              ) : (
                <Image
                  style={{
                    left: this.state.startingX,
                    top: this.state.startingY,
                    width: '97%',
                  }}
                  source={require('../Images/Blob2.png')}
                  resizeMode="center"></Image>
              )} */}

              <Text
                style={{
                  color: '#0F2146',
                  fontSize: 30,
                  fontFamily: 'NunitoSans-Bold',
                  textAlign: 'center',
                  position: 'relative',
                  marginTop: 50,
                }}>
                Build your trusted {'\n'} network
              </Text>
              <Text
                style={{
                  color: '#0F2146',
                  fontSize: 18,
                  fontFamily: 'NunitoSans-Regular',
                  textAlign: 'center',
                  position: 'relative',
                  alignItems: 'center',
                  alignSelf: 'center',
                  margin: 20,
                }}>
                Select and invite your friends with the best taste.
              </Text>
              <View
                style={{
                  margin: 10,
                  alignSelf: 'center',
                  flexDirection: 'row',
                  position: 'relative',
                }}>
                <View
                  style={{
                    height: 10,
                    width: 10,
                    borderRadius: 10 / 2,
                    backgroundColor: '#F2F5F8',
                    borderWidth: 2,
                    borderColor: '#A6AFBD',
                    margin: 5,
                  }}></View>
                <View
                  style={{
                    height: 6,
                    width: 6,
                    borderRadius: 6 / 2,
                    backgroundColor: '#A6AFBD',
                    marginTop: 7,
                    margin: 5,
                  }}></View>

                <View
                  style={{
                    height: 10,
                    width: 10,
                    borderRadius: 10 / 2,
                    backgroundColor: '#F2F5F8',
                    borderWidth: 2,
                    borderColor: '#A6AFBD',
                    margin: 5,
                  }}></View>
              </View>
              <Text
                style={{
                  color: '#0F2146',
                  fontSize: 14,
                  fontFamily: 'NunitoSans-Regular',
                  textAlign: 'center',

                  alignItems: 'center',
                  margin: 20,
                  marginTop: 20,
                  alignSelf: 'center',
                }}
                onPress={() => this.props.navigation.navigate('OnBoarding_3')}>
                Skip
              </Text>
            </View>
          </ScrollView>
        </GestureRecognizer>
      </SafeAreaView>
    );
  }
}
export default Introduction;
