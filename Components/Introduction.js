import React, {Component} from 'react';
import {
  SafeAreaView,
  Image,
  ScrollView,
  View,
  Text,
  UIManager,
  LayoutAnimation,
  Animated,
  Easing,
} from 'react-native';
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';

UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);

class Introduction extends Component {
  constructor(props) {
    super(props);
    this.spinValue = new Animated.Value(0);
    this.state = {
      pageIndex: 0,
      startingX: -50,
      startingY: 10,
      changedDone: false,
    };
  }
  spin() {
    this.spinValue.setValue(0);
    Animated.timing(this.spinValue, {
      toValue: 80,
      duration: 1000,
      easing: Easing.linear,
      useNativeDriver: true,
      
    }).start();


    this.middlePosition()
  }
  startPosition = () => {
    LayoutAnimation.linear();
    this.setState({
      startingX: -50,
      startingY: 10,
    });
  };

  middlePosition = () => {
    LayoutAnimation.linear(() =>
      this.setState({
        startingX: 60,
        startingY: 10,
      }),
    );
  };

  lastPosition = () => {
    LayoutAnimation.linear();
    this.setState({
      startingX: 40,
      startingY: -150,
    });
  };

  onSwipe(gestureName) {
    const {SWIPE_UP, SWIPE_DOWN, SWIPE_LEFT, SWIPE_RIGHT} = swipeDirections;

    switch (gestureName) {
      case SWIPE_UP:
        console.log('UP');
        break;
      case SWIPE_DOWN:
        console.log('Down');
        break;
      case SWIPE_LEFT:
        console.log('left');
        if (this.state.pageIndex < 2) {
          this.setState({pageIndex: this.state.pageIndex + 1}, () => {
            console.log(this.state.pageIndex);
            if (this.state.pageIndex == 1) {
              //this.middlePosition();
              this.spin();
            } else if (this.state.pageIndex == 2) {
              this.lastPosition();
            }
          });
        }

        break;
      case SWIPE_RIGHT:
        console.log('Right');
        if (this.state.pageIndex > 0) {
          this.setState({pageIndex: this.state.pageIndex - 1}, () => {
            console.log(this.state.pageIndex);
            if (this.state.pageIndex == 0) {
              this.startPosition();
            } else if (this.state.pageIndex == 1) {
              this.middlePosition();
            }
          });
        }

        break;
    }
  }
  render() {
    const config = {
      velocityThreshold: 0.3,
      directionalOffsetThreshold: 80,
    };

    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '190deg'],
    });
    console.log('+++++', this.state.changedDone);
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#F2F5F8'}}>
        <ScrollView
          style={{backgroundColor: '#FF0000'}}
          contentContainerStyle={{flexGrow: 1}}>
          <View
            style={{
              backgroundColor: '#FFFFFF',
              flex: 1,
              flexDirection: 'column-reverse',
            }}>
            <GestureRecognizer
              style={{
                flex: 1,
              }}
              onSwipe={(direction) => this.onSwipe(direction)}
              config={config}>
              <Animated.Image
                style={{
                  left:-40,
                  top: 10,
                  width: '97%',
                
                  transform: [{rotate:spin}],
                  
                }}
                resizeMode="contain"
                source={require('../Images/Blob.png')}
              />
              {/* {this.state.pageIndex == 0 ? (
                <Image
                  style={{
                    left: this.state.startingX,
                    top: this.state.startingY,
                    width: '97%',
                    paddingBottom: 0,

                    //height:366,
                  }}
                  source={require('../Images/Blob.png')}
                  resizeMode="contain"
                  resizeMethod="scale"></Image>
              ) : this.state.pageIndex == 1 ? (
                <Image
                  style={{
                    left: this.state.startingX,
                    top: this.state.startingY,
                    width: '97%',
                    //transform: [{ rotate: '196deg' }]
                  }}
                  source={require('../Images/Blob.png')}
                  resizeMode="contain"></Image>
              ) : (
                <Image
                  style={{
                    left: this.state.startingX,
                    top: this.state.startingY,
                    width: '97%',
                  }}
                  source={require('../Images/Blob2.png')}
                  resizeMode="center"></Image>
              )} */}

              <Text
                style={{
                  color: 'black',
                  fontSize: 24,
                  fontFamily: 'Arial',
                  textAlign: 'center',
                  position: 'relative',
                  marginTop: this.state.startingY + 20,
                }}>
                Find the best{'\n'}locations for you
              </Text>
              <Text
                style={{
                  color: 'black',
                  fontSize: 16,
                  fontFamily: 'Arial',
                  textAlign: 'center',
                  position: 'relative',
                  alignItems: 'center',
                  alignSelf: 'center',
                  margin: 20,
                }}>
                Your friends can advice you best on your location to visit.
                Right?
              </Text>
              <View
                style={{
                  margin: 10,
                  alignSelf: 'center',
                  flexDirection: 'row',
                  position: 'relative',
                }}>
                {this.state.pageIndex == 0 ? (
                  <View
                    style={{
                      height: 6,
                      width: 6,
                      borderRadius: 6 / 2,
                      backgroundColor: '#A6AFBD',
                      marginTop: 7,
                      margin: 5,
                    }}></View>
                ) : (
                  <View
                    style={{
                      height: 10,
                      width: 10,
                      borderRadius: 10 / 2,
                      backgroundColor: '#F2F5F8',
                      borderWidth: 2,
                      borderColor: '#A6AFBD',
                      margin: 5,
                    }}></View>
                )}

                {this.state.pageIndex == 1 ? (
                  <View
                    style={{
                      height: 6,
                      width: 6,
                      borderRadius: 6 / 2,
                      backgroundColor: '#A6AFBD',
                      marginTop: 7,
                      margin: 5,
                    }}></View>
                ) : (
                  <View
                    style={{
                      height: 10,
                      width: 10,
                      borderRadius: 10 / 2,
                      backgroundColor: '#F2F5F8',
                      borderWidth: 2,
                      borderColor: '#A6AFBD',
                      margin: 5,
                    }}></View>
                )}

                {this.state.pageIndex == 2 ? (
                  <View
                    style={{
                      height: 6,
                      width: 6,
                      borderRadius: 6 / 2,
                      backgroundColor: '#A6AFBD',
                      marginTop: 7,
                      margin: 5,
                    }}></View>
                ) : (
                  <View
                    style={{
                      height: 10,
                      width: 10,
                      borderRadius: 10 / 2,
                      backgroundColor: '#F2F5F8',
                      borderWidth: 2,
                      borderColor: '#A6AFBD',
                      margin: 5,
                    }}></View>
                )}
              </View>
              <Text
                style={{
                  color: 'black',
                  fontSize: 16,
                  fontFamily: 'Arial',
                  textAlign: 'center',

                  alignItems: 'center',
                  margin: 20,
                  marginTop: 50,
                  alignSelf: 'center',
                }}>
                Skip
              </Text>
            </GestureRecognizer>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
export default Introduction;
