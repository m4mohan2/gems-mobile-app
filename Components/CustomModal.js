import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Modal,
  TextInput,
  TouchableOpacity,
  Image,
  Alert,
  Linking,
} from 'react-native';

export default class CustomModal extends React.Component {
  constructor(props) {
    super(props);
    console.log('kk', this.props);
  }
  componentDidMount() {}

  hideModal = () => {
    this.props.updateAleartParent();
  };

  render() {
    return (
      <Modal transparent={true}>
        <View style={styles.View_1dropDown}>
          <View
            style={{
              width: '90%',
              backgroundColor: 'white',
              justifyContent: 'center',
              alignSelf: 'center',
              alignItems: 'center',
              padding: 40,
              marginTop: 50,
              marginBottom: 50,
              paddingTop: 20,
            }}>
            <Image
              style={{width: 65, height: 58}}
              resizeMode="contain"
              source={
                this.props.newvalue.success == true
                  ? require('../Images/successIcon.png')
                  : require('../Images/errorIcon.png')
              }></Image>
            <Text
              style={[
                styles.Lbl_Style,
                {
                  fontSize: 26,
                  marginTop: 10,
                  color:
                    this.props.newvalue.success == true ? '#4fb067' : '#ff5151',
                  fontFamily: 'NunitoSans-Bold',
                },
              ]}>
              {this.props.newvalue.success == true ? 'SUCCESS' : 'SORRY'}
            </Text>

            <Text
              style={[
                styles.Lbl_Style,
                {fontSize: 20, margin: 10, color: '#252525'},
              ]}>
              {this.props.newvalue.AlertTitle}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 20,
                width: 150,
                alignSelf: 'center',
              }}>
              <TouchableOpacity
                style={{
                  flex: 1,
                  backgroundColor:
                    this.props.newvalue.success == true ? '#4fb067' : '#ff5151',
                  height: 50,
                  borderRightColor: '#fff',
                  borderRightWidth: 1,
                  justifyContent: 'center',
                  borderRadius: 20,

                  alignSelf: 'center',
                }}
                onPress={() => this.hideModal()}>
                <Text style={[styles.Lbl_Style, {fontSize: 14, color: '#fff'}]}>
                  {this.props.newvalue.AlertOkTitle}
                </Text>
              </TouchableOpacity>
              {/* <TouchableOpacity
                style={{
                  flex: 1,
                  backgroundColor: '#25bdae',
                  height: 50,
                  borderRightColor: '#fff',
                  borderRightWidth: 1,
                  justifyContent: 'center',
                  borderTopLeftRadius: 25,
                  borderBottomLeftRadius: 25,
                }}
                onPress={() => this.backBtnPress()}>
                <Text
                  style={[
                    styles.Lbl_Style,
                    {fontSize: RFValue(14), color: '#fff'},
                  ]}>
                  {this.props.newvalue.AlertCancleTitle}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  flex: 1,
                  backgroundColor: '#e40a0a',
                  height: 50,
                  justifyContent: 'center',
                  borderLeftWidth: 1,
                  borderLeftColor: '#fff',
                  borderTopRightRadius: 25,
                  borderBottomRightRadius: 25,
                }}
                onPress={() => this.doneBtnPress()}>
                <Text
                  style={[
                    styles.Lbl_Style,
                    {fontSize: RFValue(14), color: '#fff'},
                  ]}>
                  {this.props.newvalue.AlertOkTitle}
                </Text>
              </TouchableOpacity>*/}
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  View_1dropDown: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    //width: 200,
    //height: 200,
    backgroundColor: 'rgba(52, 52, 52, 0.6)',
    justifyContent: 'center',
  },
  Lbl_Style: {
    //color: '#6d6b6b',
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
  },
});
