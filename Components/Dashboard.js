import React, {Component} from 'react';
import {
  SafeAreaView,
  Image,
  ScrollView,
  View,
  Text,
  BackHandler,
  ToastAndroid,
} from 'react-native';
import {connect} from 'react-redux';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {count: 0, timer: null};
  }

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButton.bind(this),
    );
  }
  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButton.bind(this),
    );
  }
  handleBackButton() {
    if (this.state.count == 0) {
      this.setState({count: this.state.count + 1});
      ToastAndroid.show('press back again to exist the app', 300);

      setTimeout(() => {
        this.setState({count: 0});
      }, 10000);
      //   let timer = setInterval(this.tick, 10);
      // this.setState({timer});
      return true;
    } else {
      BackHandler.exitApp();
    }
  }
  tick = () => {
    this.setState({count: 0});
  };
  render() {
    // console.warn("this.props DashBoard ===>>", this.props.appdata.userDetails.user.id
    // );
    //const {first_name } = this.props.route.params;

    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#F2F5F8'}}>
        <View
          style={{
            justifyContent: 'center',
            flex: 1,
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontSize: 26,
              color: '#222529',
              fontFamily: 'NunitoSans-Bold',
            }}>
            Welcome
          </Text>

          <Text
            style={{
              fontSize: 26,
              color: '#222529',
              fontFamily: 'NunitoSans-Bold',
            }}>
            Dashboard coming soon.
          </Text>
        </View>
      </SafeAreaView>
    );
  }
}
const mapStateToProps = (state) => {
  const {appdata} = state;
  return {appdata};
};

export default connect(mapStateToProps)(Dashboard);
