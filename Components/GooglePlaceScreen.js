import React, {Component} from 'react';

import {
  SafeAreaView,
  View,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  Image,
  FlatList,
  Text,
  Dimensions,
  ToastAndroid,
  BackHandler,
} from 'react-native';
import AnimateLoadingButton from 'react-native-animate-loading-button';

class GooglePlaceSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchPlace: [],
      count: 0,
    };

    console.log(this.props.route.params?.selection);
  }

  goProfile = () => {
    this.props.navigation.navigate('Profile', {
      refresh: false,
    });
  };

  searchText(text) {
    console.log(text);
    if (text.length > 2) {
      const url =
        'https://maps.googleapis.com/maps/api/place/textsearch/json?query=' +
        text +
        'type=restaurant&key=AIzaSyDC7qyMTTAr0YzGKCKMU4vje4uKfD8lKso';

      console.log(url);

      fetch(url)
        .then((response) => response.json())
        .then((JsonResponse) => {
          console.log(JsonResponse);
          if (JsonResponse.status == 'OK') {
            this.setState({searchPlace: JsonResponse.results});
          }
        })
        .catch((error) => {
          alert('error');
        });

      //https://maps.googleapis.com/maps/api/place/textsearch/xml?query=restaurants+in+Sydney&key=YOUR_API_KEY
    } else {
      this.setState({searchPlace: []});
    }
  }

  _onPress = (item) => {
    console.log(item);

    this.props.navigation.navigate('AddGems', {
      selection: {
        current_lat: item.geometry.location.lat,
        current_lng: item.geometry.location.lng,
        plot: item,
      },
    });
  };
  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButton.bind(this),
    );
  }
  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButton.bind(this),
    );
  }
  handleBackButton() {
    if (this.state.count == 0) {
      this.setState({count: this.state.count + 1});
      ToastAndroid.show('press back again to exist the app', 300);

      setTimeout(() => {
        this.setState({count: 0});
      }, 10000);
      //   let timer = setInterval(this.tick, 10);
      // this.setState({timer});
      return true;
    } else {
      BackHandler.exitApp();
    }
  }
  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#191C1F'}}>
        <View
          style={{
            flex: 1,
            // alignItems: 'center',
            // justifyContent: 'center',
            backgroundColor: '#F2F5F8',
          }}>
          <View
            style={{
              height: 55,
              margin: 10,
              marginTop: 20,
              borderRadius: 10,
              backgroundColor: '#FFFFFF',
              flexDirection: 'row-reverse',
            }}>
            <TouchableOpacity
              style={{
                height: 55,
                marginTop: 0,
                marginEnd: 0,
                width: 60,
                //backgroundColor: 'red',
              }}
              onPress={this.goProfile}>
              <Image
                style={{
                  width: 40,
                  height: 37,
                  marginTop: 10,
                }}
                source={require('../Images/Profile.png')}
                resizeMode="contain"
                resizeMethod="scale"></Image>
            </TouchableOpacity>
            {/* <View
              style={{
                width: 1,
                height: 35,
                backgroundColor: '#CCD2D9',
                margin: 10,
              }}></View> */}
            <TextInput
              style={{
                left: 0,
                marginTop: 0,
                marginRight: 10,
                marginBottom: 0,
                marginLeft: 0,
                flex: 1,
                paddingRight: 10,
                paddingLeft: 20,
                fontSize: 17,
                fontFamily: 'NunitoSans-Regular',
              }}
              placeholder="Search..."
              autoFocus={true}
              placeholderTextColor="#0F2146"
              onChangeText={(text) => this.searchText(text)}></TextInput>
          </View>
          <View style={{margin: 10, marginTop: 0}}>
            <FlatList
              data={this.state.searchPlace}
              renderItem={({item}) => (
                <TouchableOpacity
                  style={{backgroundColor: 'transparent'}}
                  onPress={() => this._onPress(item)}>
                  <View style={RegisterStyles.item}>
                    <Text style={RegisterStyles.title}>{item.name}</Text>
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>

          <View
            style={{
              //flex: 1,
              backgroundColor: 'rgb(255,255,255)',
              justifyContent: 'center',
              margin: 10,
              marginTop: 80,
              marginBottom: 0,
            }}>
            <AnimateLoadingButton
              ref={(c) => (this.joinNowButton = c)}
              height={60}
              width={Dimensions.get('window').width - 20}
              title="Add Gem manually"
              titleFontSize={18}
              titleColor="rgb(255,255,255)"
              backgroundColor="#3D73DD"
              borderRadius={10}
              titleFontFamily={'NunitoSans-Regular'}
              onPress={() => this.props.navigation.navigate('GemAddForm')}
              //onPress={this.loginService()}
              //onPress={this._onPressHandler.bind(this)}
              //style={{fontFamily:'Arial'}}
            />
          </View>
          <View
            style={{
              //flex: 1,
              backgroundColor: 'rgb(255,255,255)',
              justifyContent: 'center',
              margin: 10,
              marginTop: 20,
            }}>
            <AnimateLoadingButton
              ref={(c) => (this.joinNowButton = c)}
              height={60}
              width={Dimensions.get('window').width - 20}
              title="Drop Gem on map"
              titleFontSize={18}
              titleColor="rgb(255,255,255)"
              backgroundColor="#3D73DD"
              borderRadius={10}
              titleFontFamily={'NunitoSans-Regular'}
              onPress={() =>
                this.props.navigation.navigate('DraggableMarker', {
                  lat: this.props.route.params?.lat,
                  lng: this.props.route.params?.lng,
                })
              }
              //onPress={this.loginService()}
              //onPress={this._onPressHandler.bind(this)}
              //style={{fontFamily:'Arial'}}
            />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const RegisterStyles = StyleSheet.create({
  inputStyleBox: {
    height: 60,
    margin: 20,
    backgroundColor: '#F2F5F8',
    borderRadius: 5,
    fontSize: 17,
    fontFamily: 'NunitoSans-Regular',
    padding: 20,
    color: '#0F2146',
    marginBottom: 0,
  },
  container: {
    flex: 1,
  },
  item: {
    backgroundColor: 'white',
    padding: 10,
  },
  title: {
    fontSize: 16,
    color: '#0F2146',
  },
});

export default GooglePlaceSearch;
