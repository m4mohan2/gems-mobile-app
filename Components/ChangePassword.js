import React, {Component} from 'react';
import {
  SafeAreaView,
  KeyboardAvoidingView,
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Dimensions,
  Image,
  BackHandler,
  ToastAndroid,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import AnimateLoadingButton from 'react-native-animate-loading-button';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {connect} from 'react-redux';
import CustomModal from './CustomModal';

const initialValues = {
  old_password: '',
  new_password: '',
  confirm_password: '',
};

const validationRegister = Yup.object().shape({
  old_password: Yup.string().required('Password field is required.'),
  new_password: Yup.string().required('New password field is required.'),
  confirm_password: Yup.string()
    .required('Confirm password field is required.')
    .test(
      'passwords-match',
      'Password and Confirm password must match',
      function (value) {
        return this.parent.new_password === value;
      },
    ),
});
class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {showCustomModal: false, count: 0};
  }
  update_Custom_modal = () => {
    this.setState({showCustomModal: false});
  };
  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButton.bind(this),
    );
  }
  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButton.bind(this),
    );
  }
  handleBackButton() {
    if (this.state.count == 0) {
      this.setState({count: this.state.count + 1});
      ToastAndroid.show('press back again to exist the app', 300);

      setTimeout(() => {
        this.setState({count: 0});
      }, 10000);
      //   let timer = setInterval(this.tick, 10);
      // this.setState({timer});
      return true;
    } else {
      BackHandler.exitApp();
    }
  }
  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#222529'}}>
        <KeyboardAvoidingView
          style={{flex: 1}}
          behavior={Platform.OS == 'ios' ? 'padding' : 'height'}>
          <ScrollView style={{}}>
            <View style={{flex: 1, backgroundColor: '#222529'}}>
              <View
                style={{
                  flexDirection: 'row',
                  //justifyContent: 'space-between',
                  //backgroundColor: 'blue',
                }}>
                <Text
                  style={{
                    flex: 1,
                    textAlign: 'center',
                    color: 'white',
                    fontSize: 32,
                    marginTop: 22,
                    alignSelf: 'center',
                    fontFamily: 'NunitoSans-ExtraBold',
                  }}>
                  gems
                </Text>
                <TouchableOpacity
                  style={{
                    position: 'absolute',
                    marginLeft: 20,
                    height: 20,
                    width: 30,
                    marginTop: 41,
                  }}
                  onPress={() => {
                    this.props.navigation.navigate('EditProfile');
                  }}>
                  <Image
                    style={{height: 20, width: 20}}
                    source={require('../Images/back.png')}
                    resizeMode="contain"></Image>
                </TouchableOpacity>
              </View>
              <Formik
                initialValues={initialValues}
                validationSchema={validationRegister}
                onSubmit={(values, actions) => {
                  this.joinNowButton.showLoading(true);
                  console.log(values);
                  AsyncStorage.getItem('userInfo').then((value) => {
                    const jsonValue = JSON.parse(value);

                    fetch(
                      this.props.appdata.RELEASE_API_URL + 'api/updatePassword',
                      {
                        method: 'POST',
                        headers: {
                          token: 'Bearer ' + jsonValue.token,
                          'Content-Type': 'application/json',
                        },

                        body: JSON.stringify({
                          old_password: values.old_password,
                          new_password: values.new_password,
                          confirm_password: values.confirm_password,
                        }),
                      },
                    )
                      .then((response) => response.json())
                      .then((responseJson) => {
                        this.joinNowButton.showLoading(false);

                        if (responseJson.success == true) {
                          this.setState({
                            showCustomModal: true,
                            AlertValue: {
                              AlertHedingTitle: 'gems',
                              AlertTitle: 'Your password updated successfully',
                              AlertCancleTitle: 'GO BACK',
                              success: true,
                              AlertOkTitle: 'OK',
                              SingleButton: true,
                            },
                          });
                        } else {
                          console.log(responseJson);
                          if (responseJson.message == null || undefined) {
                            actions.setErrors(responseJson);
                          } else {
                            actions.old_password.setErrors(responseJson);
                          }

                          this.joinNowButton.showLoading(false);
                        }
                      })
                      .catch((error) => {
                        console.log(error);
                        this.setState({
                          showCustomModal: true,
                          AlertValue: {
                            AlertHedingTitle: 'gems',
                            AlertTitle: responseJson.message,
                            AlertCancleTitle: 'GO BACK',
                            success: false,
                            AlertOkTitle: 'OK',
                            SingleButton: true,
                          },
                        });
                        this.joinNowButton.showLoading(false);
                      });
                  });
                }}>
                {(formikProps) => (
                  <View
                    style={{
                      flex: 1,
                      backgroundColor: '#F2F5F8',
                      marginTop: 14,
                      borderTopLeftRadius: 25,
                      borderTopRightRadius: 25,
                    }}>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222529',
                        fontSize: 30,
                        marginTop: 40,
                        marginLeft: 24,
                        //alignSelf: 'center',
                        textAlign: 'left',
                        fontFamily: 'NunitoSans-Bold',
                      }}>
                      Change password
                    </Text>
                    <TextInput
                      style={RegisterStyles.inputStyleBox}
                      placeholderTextColor="#0F2146"
                      placeholder="Current password"
                      //underlineColorAndroid="rgba(0,0,0,0)"
                      keyboardType="default"
                      returnKeyType="next"
                      fontFamily="NunitoSans-Regular"
                      //onChangeText={(text) => this.setState({userName: text})}
                      value={formikProps.values.old_password}
                      onChangeText={formikProps.handleChange('old_password')}
                      onBlur={formikProps.handleBlur('old_password')}
                      blurOnSubmit={false}
                      autoCorrect={false}
                      disableFullscreenUI={true}
                      autoCapitalize="none"
                      onSubmitEditing={() => {
                        this.secondTextInput.focus();
                      }}

                      //onBlur={() => this._CheckTConditionChacked()}
                      //onFocus={() => this._onFocusChacked()}
                    />

                    {formikProps.touched.old_password &&
                    formikProps.errors.old_password ? (
                      <Text style={RegisterStyles.errMsg}>
                        {formikProps.touched.old_password &&
                          formikProps.errors.old_password}
                      </Text>
                    ) : null}

                    <TextInput
                      ref={(input) => {
                        this.secondTextInput = input;
                      }}
                      style={RegisterStyles.inputStyleBox}
                      placeholderTextColor="#0F2146"
                      placeholder="New password"
                      //underlineColorAndroid="rgba(0,0,0,0)"
                      keyboardType="default"
                      returnKeyType="next"
                      //onChangeText={(text) => this.setState({userName: text})}
                      value={formikProps.values.new_password}
                      autoCorrect={false}
                      disableFullscreenUI={true}
                      onSubmitEditing={() => {
                        this.thirdTextInput.focus();
                      }}
                      autoCapitalize="none"
                      onChangeText={formikProps.handleChange('new_password')}
                      onBlur={formikProps.handleBlur('new_password')}
                      blurOnSubmit={false}
                      //onBlur={() => this._CheckTConditionChacked()}
                      //onFocus={() => this._onFocusChacked()}
                    />
                    {formikProps.touched.new_password &&
                    formikProps.errors.new_password ? (
                      <Text style={RegisterStyles.errMsg}>
                        {formikProps.touched.new_password &&
                          formikProps.errors.new_password}
                      </Text>
                    ) : null}
                    <TextInput
                      ref={(input) => {
                        this.thirdTextInput = input;
                      }}
                      style={RegisterStyles.inputStyleBox}
                      placeholderTextColor="#0F2146"
                      placeholder="Type new password again"
                      //underlineColorAndroid="rgba(0,0,0,0)"
                      keyboardType="default"
                      returnKeyType="next"
                      //onChangeText={(text) => this.setState({userName: text})}
                      value={formikProps.values.confirm_password}
                      autoCorrect={false}
                      disableFullscreenUI={true}
                      autoCapitalize="none"
                      onSubmitEditing={() => {
                        this.fourthTextInput.focus();
                      }}
                      blurOnSubmit={false}
                      //onBlur={() => this._CheckTConditionChacked()}
                      //onFocus={() => this._onFocusChacked()}
                      onChangeText={formikProps.handleChange(
                        'confirm_password',
                      )}
                      onBlur={formikProps.handleBlur('confirm_password')}
                    />
                    {formikProps.touched.confirm_password &&
                    formikProps.errors.confirm_password ? (
                      <Text style={RegisterStyles.errMsg}>
                        {formikProps.touched.confirm_password &&
                          formikProps.errors.confirm_password}
                      </Text>
                    ) : null}
                    <View
                      style={{
                        backgroundColor: '#F2F5F8',
                        justifyContent: 'center',
                        margin: 20,
                        marginTop: 30,
                      }}>
                      <AnimateLoadingButton
                        ref={(c) => (this.joinNowButton = c)}
                        height={60}
                        width={Dimensions.get('window').width - 40}
                        title="Save"
                        titleFontSize={18}
                        titleColor="rgb(255,255,255)"
                        backgroundColor="#3D73DD"
                        borderRadius={10}
                        titleFontFamily={'NunitoSans-Regular'}
                        onPress={formikProps.handleSubmit}
                        //onPress={this.loginService()}
                        //onPress={this._onPressHandler.bind(this)}
                        //style={{fontFamily:'Arial'}}
                      />
                    </View>
                  </View>
                )}
              </Formik>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
        {this.state.showCustomModal ? (
          <CustomModal
            newvalue={this.state.AlertValue}
            updateAleartParent={this.update_Custom_modal.bind(this)}
          />
        ) : null}
      </SafeAreaView>
    );
  }
}
const RegisterStyles = StyleSheet.create({
  inputStyleBox: {
    height: 60,
    margin: 20,
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
    fontSize: 17,
    fontFamily: 'Arial',
    padding: 20,
    color: '#0F2146',
    marginBottom: 0,
    fontFamily: 'NunitoSans-Regular',
  },
  errMsg: {
    color: 'red',

    fontSize: 16,
    letterSpacing: 1,
    marginTop: 5,
    marginLeft: 20,
  },
});
const mapStateToProps = (state) => {
  const {appdata} = state;
  return {appdata};
};

export default connect(mapStateToProps)(ChangePassword);
