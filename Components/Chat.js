import React, {Component} from 'react';

import {SafeAreaView, View, Text} from 'react-native';

class ForYou extends Component {
  render() {
    return (
      <SafeAreaView>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontSize: 26,
              color: '#222529',
              fontFamily: 'NunitoSans-Bold',
            }}>
            Chat
          </Text>
        </View>
      </SafeAreaView>
    );
  }
}

export default ForYou;
