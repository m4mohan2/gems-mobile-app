import 'react-native-gesture-handler';
import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import {connect} from 'react-redux';

import CreateAccountScreen from './CreateAccount';
import WelcomeScreen from './Welcome';
import LoginScreen from './Login';
import OnBoarding_1Screen from './OnBoarding1';
import OnBoarding_2Screen from './OnBoarding2';
import OnBoarding_3Screen from './OnBoarding3';
import ForYouScreen from './ForYou';
import ExploreScreen from './Explore';
import AddGemsScreen from './AddGems';
import ChatScreen from './Chat';
import ProfileScreen from './profile';
import EditProfileScreen from './EditProfile';
import ChangePasswordScreen from './ChangePassword';
import GooglePlaceSearchScreen from './GooglePlaceScreen';
import GemAddFormScreen from './AddGemForm';
import ForgetPasswordScreen from './ForgetPassword';
import DraggableMarkerScreen from './DraggableMarker';

import {Easing, Image} from 'react-native';

const config = {
  animation: 'timing',
  config: {
    duration: 3000,

    Easing: Easing.out(Easing.ease()),
  },
};

const config1 = {
  animation: 'timing',
  config: {
    duration: 1000,

    Easing: Easing.out(Easing.ease()),
  },
};
const forFade = ({current}) => ({
  cardStyle: {
    opacity: current.progress,
  },
});

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function DashBoard() {
  return (
    <Tab.Navigator
      initialRouteName={'AddGems'}
      keyboardHidesTabBar={true}
      screenOptions={({route}) => ({
        tabBarIcon: ({focused}) => {
          if (route.name === 'ForYou') {
            return (
              <Image
                source={
                  focused
                    ? require('../Images/for_you.png')
                    : require('../Images/for_you_gray.png')
                }
                style={{
                  width: 20,
                  height: 20,
                }}
              />
            );
          } else if (route.name === 'Explore') {
            return (
              <Image
                source={
                  focused
                    ? require('../Images/Explore_white.png')
                    : require('../Images/Explore.png')
                }
                style={{
                  width: 20,
                  height: 20,
                }}
              />
            );
          } else if (route.name === 'AddGems') {
            return (
              <Image
                source={
                  focused
                    ? require('../Images/add_gem_white.png')
                    : require('../Images/add_gem.png')
                }
                style={{
                  width: 20,
                  height: 20,
                }}
              />
            );
          } else if (route.name === 'Chat') {
            return (
              <Image
                source={
                  focused
                    ? require('../Images/chat_white.png')
                    : require('../Images/chat.png')
                }
                style={{
                  width: 20,
                  height: 20,
                }}
              />
            );
          }

          // You can return any component that you like here!
        },
      })}
      tabBarOptions={{
        activeTintColor: 'white',
        inactiveTintColor: '#6D6D6D',
        labelPosition: 'below-icon',
        labelStyle: {
          fontSize: 13,
          fontFamily: 'NunitoSans-SemiBold',
          marginBottom: 15,
        },

        style: {
          backgroundColor: '#191C1F', // TabBar background
          height: 70,
          borderTopColor: 'transparent',
        },
      }}>
      <Tab.Screen name="ForYou" component={ForYouScreen} />
      <Tab.Screen name="Explore" component={ExploreScreen} />
      <Tab.Screen name="AddGems" component={AddGemsScreen} />
      <Tab.Screen name="Chat" component={ChatScreen} />
    </Tab.Navigator>
  );
}

function App(userInfo) {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={
          userInfo.userInfo.userData == null ? 'OnBoarding_1' : 'DashBoard'
        }>
        <Stack.Screen
          name="CreateAccount"
          component={CreateAccountScreen}
          options={{
            headerShown: false,
            cardStyleInterpolator: forFade,
            transitionSpec: {
              open: config1,
              close: config1,
            },
          }}
        />
        <Stack.Screen
          name="OnBoarding_1"
          component={OnBoarding_1Screen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="OnBoarding_2"
          component={OnBoarding_2Screen}
          options={{
            headerShown: false,
            cardStyleInterpolator: forFade,
            transitionSpec: {
              open: config,
              close: config,
            },
          }}
        />
        <Stack.Screen
          name="OnBoarding_3"
          component={OnBoarding_3Screen}
          options={{
            headerShown: false,
            cardStyleInterpolator: forFade,
            transitionSpec: {
              open: config,
              close: config,
            },
          }}
        />
        <Stack.Screen
          name="Welcome"
          component={WelcomeScreen}
          options={{
            headerShown: false,
            cardStyleInterpolator: forFade,
            transitionSpec: {
              open: config1,
              close: config1,
            },
          }}
        />
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{
            headerShown: false,
            cardStyleInterpolator: forFade,
            transitionSpec: {
              open: config1,
              close: config1,
            },
          }}
        />

        <Stack.Screen
          name="DashBoard"
          component={DashBoard}
          options={{
            headerShown: false,
            cardStyleInterpolator: forFade,
            transitionSpec: {
              open: config1,
              close: config1,
            },
          }}
        />

        <Stack.Screen
          name="Profile"
          component={ProfileScreen}
          options={{
            headerShown: false,
            cardStyleInterpolator: forFade,
            transitionSpec: {
              open: config1,
              close: config1,
            },
          }}
        />

        <Stack.Screen
          name="EditProfile"
          component={EditProfileScreen}
          options={{
            headerShown: false,
            cardStyleInterpolator: forFade,
          }}
        />

        <Stack.Screen
          name="ChangePassword"
          component={ChangePasswordScreen}
          options={{
            headerShown: false,
            cardStyleInterpolator: forFade,
          }}
        />

        <Stack.Screen
          name="GooglePlaceSearch"
          component={GooglePlaceSearchScreen}
          options={{
            headerShown: false,
            cardStyleInterpolator: forFade,
          }}
        />
        <Stack.Screen
          name="GemAddForm"
          component={GemAddFormScreen}
          options={{
            headerShown: false,
            cardStyleInterpolator: forFade,
          }}
        />
        <Stack.Screen
          name="ForgetPassword"
          component={ForgetPasswordScreen}
          options={{
            headerShown: false,
            cardStyleInterpolator: forFade,
            transitionSpec: {
              open: config1,
              close: config1,
            },
          }}
        />
        <Stack.Screen
          name="DraggableMarker"
          component={DraggableMarkerScreen}
          options={{
            headerShown: false,
            cardStyleInterpolator: forFade,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const mapStateToProps = (state) => {
  const {appdata} = state;
  return {appdata};
};

export default connect(mapStateToProps)(App);
