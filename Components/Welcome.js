import React, {Component} from 'react';
import {
  SafeAreaView,
  Image,
  ScrollView,
  View,
  Text,
  StatusBar,
  Dimensions,
  Animated,
  Easing,
} from 'react-native';
var deviceHeight = Dimensions.get('window').height;
let opacity = new Animated.Value(0);
let opacityRight = new Animated.Value(0);

const animate = (easing) => {
  opacity.setValue(0);
  Animated.timing(opacity, {
    toValue: 1,
    duration: 500,
    easing,
    useNativeDriver: false,
  }).start();
};

const animateRightButton = (easing) => {
  opacityRight.setValue(0);
  Animated.timing(opacityRight, {
    toValue: 0.2,
    duration: 500,
    easing,
  }).start();
};
const size = opacity.interpolate({
  inputRange: [0, 1],
  outputRange: ['-100%', '-26%'],
});

const sizeRightButton = opacityRight.interpolate({
  inputRange: [0, 1],
  outputRange: ['100%', '-100%'],
});

class Introduction extends Component {
  constructor(props) {
    super(props);
    console.log(this.props);
    state = {
      timer: null,
     
    };
  
  }

  componentDidMount() {
    animate();
    animateRightButton();
    let timer = setInterval(this.tick, 10000);
    this.setState({timer});
  }
  componentWillUnmount() {
    clearInterval(this.state.timer);
  }
  tick =() => {
    
    this.props.navigation.navigate('Login')
    clearInterval(this.state.timer);
  }

  render() {
            
    const {first_name } = this.props.route.params;
    console.log(first_name);

    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#F2F5F8'}}>
        <StatusBar barStyle="dark-content" />
        <ScrollView style={{}}>
          <View
            style={{
              backgroundColor: '#FFFFFF',
              flex: 6,
              flexDirection: 'column',
            }}>
            <Text
              style={{
                marginTop: 30,
                alignSelf: 'center',
                fontSize: 32,
                marginBottom: 0,
                fontFamily: 'NunitoSans-ExtraBold'
              }}>
              {' '}
              gems
            </Text>

            <Animated.Image
              style={{
                marginRight: '25%',
                left: size,
                margin: 0,
                width: '100%',
                marginTop: 0,
                height: deviceHeight / 2,
                //backgroundColor: 'red',
              }}
              source={require('../Images/Blob.png')}
              resizeMode="contain"></Animated.Image>

            <Animated.Image
              style={{
                position: 'absolute',
                //right: '40%',
               //marginRight:'-40',
                left: sizeRightButton,
                
                width: 198,
                marginTop: 0,
                opacity: 0.2,
                height: '25%',
                top: 30,
                transform: [{rotate: '175deg'}],
              }}
              source={require('../Images/Blob.png')}
              resizeMode="contain"></Animated.Image>

            <Text
              style={{
                fontSize: 30,
                color: '#0F2146',
                alignSelf: 'center',
                marginTop: 50,
                fontFamily: 'NunitoSans-Bold'
              }}>
              Welcome, {first_name}!
            </Text>

            <Text
              style={{
                margin: 20,
                fontSize: 18,
                color: '#0F2146',
                alignSelf: 'center',
                fontFamily: 'NunitoSans-Bold'
              }}>
              Thanks for your registration.
              <Text style={{color: '#0F2146', fontFamily: 'NunitoSans-Regular'}}>
                {' '}
                We have send you an email to confirm your email address. Please
                click the link in the email to start using Gems.
              </Text>
            </Text>
          </View>
        </ScrollView>
      </SafeAreaView>

      //   <SafeAreaView style={{flex: 1, backgroundColor: '#F2F5F8'}}>
      //     <ScrollView
      //       style={{backgroundColor: '#FF0000'}}
      //       contentContainerStyle={{flexGrow: 1}}>
      //       <View
      //         style={{
      //           backgroundColor: '#FFFFFF',
      //           flex: 1,
      //           flexDirection: 'column-reverse',
      //         }}>
      //         <Image
      //           style={{
      //             left: this.state.startingX,
      //             top: this.state.startingY,
      //             width: '97%',
      //             paddingBottom: 0,

      //           }}
      //           source={require('../Images/Blob.png')}
      //           resizeMode="contain"
      //           resizeMethod="scale"></Image>
      //         {/* {this.state.pageIndex == 0 ? (
      //             <Image
      //               style={{
      //                 left: this.state.startingX,
      //                 top: this.state.startingY,
      //                 width: '97%',
      //                 paddingBottom: 0,

      //                 //height:366,
      //               }}
      //               source={require('../Images/Blob.png')}
      //               resizeMode="contain"
      //               resizeMethod="scale"></Image>
      //           ) : this.state.pageIndex == 1 ? (
      //             <Image
      //               style={{
      //                 left: this.state.startingX,
      //                 top: this.state.startingY,
      //                 width: '97%',
      //                 //transform: [{ rotate: '196deg' }]
      //               }}
      //               source={require('../Images/Blob.png')}
      //               resizeMode="contain"></Image>
      //           ) : (
      //             <Image
      //               style={{
      //                 left: this.state.startingX,
      //                 top: this.state.startingY,
      //                 width: '97%',
      //               }}
      //               source={require('../Images/Blob2.png')}
      //               resizeMode="center"></Image>
      //           )} */}

      //         <Text
      //           style={{
      //             color: 'black',
      //             fontSize: 24,
      //             fontFamily: 'Arial',
      //             textAlign: 'center',
      //             position: 'relative',
      //             marginTop: this.state.startingY + 20,
      //           }}>
      //           Find the best{'\n'}locations for you
      //         </Text>
      //         <Text
      //           style={{
      //             color: 'black',
      //             fontSize: 16,
      //             fontFamily: 'Arial',
      //             textAlign: 'center',
      //             position: 'relative',
      //             alignItems: 'center',
      //             alignSelf: 'center',
      //             margin: 20,
      //           }}>
      //           Your friends can advice you best on your location to visit. Right?
      //         </Text>
      //         <View
      //           style={{
      //             margin: 10,
      //             alignSelf: 'center',
      //             flexDirection: 'row',
      //             position: 'relative',
      //           }}>
      //           {this.state.pageIndex == 0 ? (
      //             <View
      //               style={{
      //                 height: 6,
      //                 width: 6,
      //                 borderRadius: 6 / 2,
      //                 backgroundColor: '#A6AFBD',
      //                 marginTop: 7,
      //                 margin: 5,
      //               }}></View>
      //           ) : (
      //             <View
      //               style={{
      //                 height: 10,
      //                 width: 10,
      //                 borderRadius: 10 / 2,
      //                 backgroundColor: '#F2F5F8',
      //                 borderWidth: 2,
      //                 borderColor: '#A6AFBD',
      //                 margin: 5,
      //               }}></View>
      //           )}

      //           {this.state.pageIndex == 1 ? (
      //             <View
      //               style={{
      //                 height: 6,
      //                 width: 6,
      //                 borderRadius: 6 / 2,
      //                 backgroundColor: '#A6AFBD',
      //                 marginTop: 7,
      //                 margin: 5,
      //               }}></View>
      //           ) : (
      //             <View
      //               style={{
      //                 height: 10,
      //                 width: 10,
      //                 borderRadius: 10 / 2,
      //                 backgroundColor: '#F2F5F8',
      //                 borderWidth: 2,
      //                 borderColor: '#A6AFBD',
      //                 margin: 5,
      //               }}></View>
      //           )}

      //           {this.state.pageIndex == 2 ? (
      //             <View
      //               style={{
      //                 height: 6,
      //                 width: 6,
      //                 borderRadius: 6 / 2,
      //                 backgroundColor: '#A6AFBD',
      //                 marginTop: 7,
      //                 margin: 5,
      //               }}></View>
      //           ) : (
      //             <View
      //               style={{
      //                 height: 10,
      //                 width: 10,
      //                 borderRadius: 10 / 2,
      //                 backgroundColor: '#F2F5F8',
      //                 borderWidth: 2,
      //                 borderColor: '#A6AFBD',
      //                 margin: 5,
      //               }}></View>
      //           )}
      //         </View>
      //         <Text
      //           style={{
      //             color: 'black',
      //             fontSize: 16,
      //             fontFamily: 'Arial',
      //             textAlign: 'center',

      //             alignItems: 'center',
      //             margin: 20,
      //             marginTop: 50,
      //             alignSelf: 'center',
      //           }}>
      //           Skip
      //         </Text>
      //       </View>
      //     </ScrollView>
      //   </SafeAreaView>
      //
    );
  }
}
export default Introduction;
